<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin','middleware'=>'auth'], function () {
	Route::get('/','adminController@index');
	// setting 
	Route::get('/websetting','WebSitSettingController@index');
	
	// add setting
	Route::post('/mainsetting','WebSitSettingController@store');
	
	// edit setting
	Route::post('/editmainsetting','WebSitSettingController@store');
	Route::post('/editmainsetting/{id}','WebSitSettingController@update');
	
     // Main category get and post
	Route::get('/maincategory','categoryController@index');
	Route::get('/categoryAdd','categoryController@create');

	Route::post('/add_main_category','categoryController@store');

	// update main caret

	Route::get('/update_main_caret/{id}','categoryController@edit');
	Route::post('/update_main_category/{id}','categoryController@update');

	// show each one of main cat

	Route::get('/main_cat_show/{id}','categoryController@show');

	// Delete main category

	Route::get('/delete_main_caret/{id}','categoryController@destroy');	



	// sub category
	Route::get('/subCategory','sub_categoryController@index');

	// sub cat add

	Route::get('/sub_cat_Add','sub_categoryController@create');
	Route::post('/add_sub_category','sub_categoryController@store');

	// sub cat edit
	Route::get('/update_sub_caret/{id}','sub_categoryController@edit');
	Route::post('/update_sub_category/{id}','sub_categoryController@update');
	// delete sub cat
	Route::get('/delete_sub_caret/{id}','sub_categoryController@destroy');

	// sub of sub
	Route::get('/Subofsub','subOfSubController@index');
	
	// add 
	Route::get('/add_subofsub','subOfSubController@create');
	Route::post('/add_subofsub','subOfSubController@store');
	
	// update 

	Route::get('/update_sub_of_sub/{id}','subOfSubController@edit');
	Route::post('/update_sub_of_sub/{id}','subOfSubController@update');
	
	// delete

	Route::get('/deleteSubOfSub/{id}','subOfSubController@destroy');
	// begin items

	Route::get('/items','itemController@index');

	// add itm
	Route::get('/additem','itemController@create');
	Route::get('/additemajax','itemController@create_ajax');
	Route::post('/additem','itemController@store');

	Route::post('ajax-open', ['as' => 'ajax-open', 'uses' => 'itemController@OpenOpen']);

	// update item
	Route::get('/update_item/{id}','itemController@edit');
	Route::post('/updateitem/{id}','itemController@update');
	
	// delete item
	Route::get('/delete_item/{id}','itemController@destroy');

	// users

	Route::get('/users','userController@index');
	
	// add user
	Route::get('/adduser','userController@create');
	Route::post('/adduser','userController@store');

	// update user
	Route::get('/edituser/{id}','userController@edit');
	Route::post('/updateuser/{id}','userController@update');

	// delete user
	Route::get('/deleteuser/{id}','userController@destroy');

	// comments
	Route::get('/comment','commentController@index');
	Route::get('/activecomment/{id}','commentController@active');
	Route::get('/disactivecomment/{id}','commentController@disactive');
	Route::get('/readcomment/{id}','commentController@read');
	Route::get('/unreadcommen/{id}','commentController@unread');
	Route::get('/deletecomment/{id}','commentController@destroy');
	Route::get('/commentshow/{id}','commentController@show');

	// admin about us
	Route::get('/about_us','aboutController@create');
	Route::post('/addabout','aboutController@store');
	Route::post('/editabout/{id}','aboutController@update');

	// contact us
	Route::get('/contact_us','contactController@index');
	Route::get('/watch_contact_massege/{id}','contactController@show');
	Route::get('/delete_contact_massege/{id}','contactController@destroy');
	Route::post('/sendcontact','contactController@store');
	
		// product
	Route::get('/product','ProductController@admin_index');
	
	// add product
	Route::get('/addproduct','ProductController@create');
	Route::post('/addproduct','ProductController@store');

	// update product
	Route::get('/updateproduct/{id}','ProductController@edit');
	Route::post('/updateproduct/{id}','ProductController@update');
	
	// Delete product
	Route::get('/deleteproduct/{id}','ProductController@destroy');

	// product request 
	Route::get('/orders','productRequestController@index');
	Route::get('/order/{id}','productRequestController@show');
	// delete product request
	Route::get('/deleteorder/{id}','productRequestController@destroy');

}); 

Route::get('/home',function(){
	return view('/admin.index');
});

// lang
Route::get('lang/{lang}', 'webController@switcher');
// Login
Route::get('/login','authController@form')->middleware('guest');
Route::post('/login','authController@sign_in');



Route::get('/','webController@index');

// all sub category of main category
Route::get('/allsubcategory/{slogen}','webController@all_post');

// all item form sub cat
Route::get('/posts/{slogen}','postController@allitem');
// show postg
Route::get('/post/{slogan}','postController@index');

// comment
Route::post('/postcomment','commentController@store');

// search
Route::any('/search','searchController@store');

	// product 
	Route::get('/products','ProductController@index');
	Route::get('/product/{slogan}','ProductController@product_details');

	// product Request
	Route::post('/productRequest','productRequestController@store');


	// about us

	Route::get('/about','webController@about');
	Route::get('/contact','webController@contact');
	
	// connect us

	Route::get('/logout',function(){
		Auth::logout();
		return redirect('/');
	});	