<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class main_cat extends Model
{
	// table name
    protected $table = "main_cat";

	// culomn of table
    protected $fileable = [

    	'cat_name_ar','cat_name_en','cat_desc_ar','cat_desc_en',
    	'order','seo_ar','seo_en'

    ];
}
