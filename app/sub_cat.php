<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sub_cat extends Model
{
    protected $table = "sub_cat";

    protected $fileable = [
    	'sub_cat_name_ar','sub_cat_name_en',
    	'seo_ar','seo_en','cat_desc_ar','cat_desc_en',
    	'order','main_cat_id'
    ];
}
