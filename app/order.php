<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = 'order';
    protected $fileable = [
        'product_id','first_name','last_name',
        'email','phone','note'
    ];
}
