<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subofsub extends Model
{
    protected $table = "subofsub";
    protected $fileable = [
        'sub_cat_id','sub_of_sub_name_ar','sub_of_sub_name_en',
        'desc_ar','desc_en','seo_ar','seo_en','viewers',
        'order','s_s_slogan_ar','s_s_slogan_en'
    ];
}
