<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = 'products';
    protected $fileable = [
        'product_title_ar','product_title_en','product_small_desc_ar','product_small_desc_en',
        'product_desc_ar' , 'product_desc_en' , 'product_type' , 'product_order',
        'image' , 'price' , 'product_slogan_ar' , 'product_slogan_en'
    ];
}
