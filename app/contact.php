<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contact extends Model
{
    protected $table = 'contact';
    protected $fileable = [
        'fname','lname','email',
        'address','massege'
    ];
}
