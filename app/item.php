<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use carbon\carbon;


class item extends Model
{
    protected $table = 'items';

    protected $fileable = [
    	'title_ar','title_en','desc_ar','desc_en',
    	'seo_ar','seo_en','feature','status',
    	'image','sub_sub_id'
    ];

    public function date()
    {
        return Carbon::parse()->format('m-d-Y');
    }

}
