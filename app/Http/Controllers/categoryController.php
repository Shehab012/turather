<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\main_cat;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main = main_cat::paginate(10);
        return view('Admin.main_category.index',[ 'data' => $main ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.main_category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'name_ar'     =>'required|unique:main_cat,cat_name_ar',
            'name_en'     =>'required|unique:main_cat,cat_name_en',
            // 'desc_ar'     =>'required',
            // 'desc_en'     =>'required',
            'order'       =>'required|integer|unique:main_cat,order',
            // 'status'      =>'required',
            // 'seo_ar'      =>'required',
            // 'seo_en'      =>'required'
        ];
        $messages = [
            'name_er.required'=>'يجب ادخال اسم القسم  ',
            'name_er.unique'=>'هذا الاسم متواجد بالفعل  ',
            'name_en.required'=>'يجب ادخال اسم القسم  ',
            'name_en.unique'=>'هذا الاسم متواجد بالفعل  ',
            // 'desc_ar.required'=>'يجب ادخال وصف للقسم',
            // 'desc_en.required'=>'يجب ادخال وصف للقسم',
            'order.required'=>'يجب اختيار ترتيب القسم',
            'order.integer'=>' يجب ان يكون  ترتيب القسم رقم',
            'order.unique'=>'لقد تم اختيار هذا الترتيب من قبل',
            // 'status.required'=>'يجب اختيار التفعيل',
            // 'seo_ar.required'=>'يجب ادخال الكلمات المفتاحيه',
            // 'seo_en.required'=>'يجب ادخال الكلمات المفتاحيه'
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x = new main_cat;
            $x->cat_name_ar = $request->name_ar;
            $x->cat_name_en = $request->name_en;
            $x->cat_desc_ar = $request->desc_ar;
            $x->cat_desc_en = $request->desc_en;
            $x->order       = $request->order;
            $x->status       = $request->status;
            $x->seo_ar      = $request->seo_ar;  
            $x->seo_en      = $request->seo_en;
            $x->subcat_slogen_ar = $this->make_slug($request->input('name_ar'));
            $x->subcat_slogen_en = $this->make_slug($request->input('name_en'));
            $x->save();
            return back()->with('success','تمت اضافه قسم رئيسي');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $x = main_cat::find($id);
        return view('Admin.main_category.show',['data' => $x]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $x = main_cat::find($id);
        return view('Admin.main_category.edit',[ 'data' => $x ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $rules = [
            'name_ar'     =>'required',
            'name_en'     =>'required',
            // 'desc_ar'     =>'required',
            // 'desc_en'     =>'required',
            'order'       =>'required|integer',
            // 'status'      =>'required',
            // 'seo_ar'      =>'required',
            // 'seo_en'      =>'required'
        ];
        $messages = [
            'name_er.required'=>'يجب ادخال اسم القسم  ',
            // 'name_er.unique'=>'هذا الاسم متواجد بالفعل  ',
            // 'name_en.required'=>'يجب ادخال اسم القسم  ',
            'name_en.unique'=>'هذا الاسم متواجد بالفعل  ',
            // 'desc_ar.required'=>'يجب ادخال وصف للقسم',
            // 'desc_en.required'=>'يجب ادخال وصف للقسم',
            'order.required'=>'يجب اختيار ترتيب القسم',
            'order.integer'=>' يجب ان يكون  ترتيب القسم رقم',
            // 'order.unique'=>'لقد تم اختيار هذا الترتيب من قبل',
            // 'status.required'=>'يجب اختيار التفعيل',
            // 'seo_ar.required'=>'يجب ادخال الكلمات المفتاحيه',
            // 'seo_en.required'=>'يجب ادخال الكلمات المفتاحيه'
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x = main_cat::find($id);
            $x->id = $id;
            $x->cat_name_ar = $request->name_ar;
            $x->cat_name_en = $request->name_en;
            $x->cat_desc_ar = $request->desc_ar;
            $x->cat_desc_en = $request->desc_en;
            $x->order       = $request->order;
            $x->status      = $request->status;        
            $x->seo_ar      = $request->seo_ar;  
            $x->seo_en      = $request->seo_en;
            $x->update();
            return back()->with('success','تم تعديل قسم رئيسي');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $x = main_cat::find($id);
        $x->delete();
        return back()->with('error','تمت ازاله قسم رئيسي');        
    }
}
