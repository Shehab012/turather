<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\comment;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class commentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = comment::paginate(10);
        return view('Admin.comment.index',['data'=>$comments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'name'    => 'required|min:4',
            'email'   =>'email',
            'massege' => 'required'
        ];
        $messages = [
            'name.required' => 'يجب ادخال اسم للمستخدم',
            'email.email' => 'خطئ ف ادخال البريد الالكتروني',
            'massege.required' => 'يجب ادخل نص الرساله'
        ];
            $Validator = Validator::make($data, $rules, $messages);
            if ($Validator->fails()) {
                return redirect()->back()->withErrors($Validator)->withInput(Input::all());
            } else {
                $x = new comment;
                $x->item_id = $request->item_id;
                $x->desc    = $request->massege;
                $x->name    = $request->name;
                $x->email   = $request->email;
                $x->save();
                return back()->with('success','تمت ارسال تعليق الي الادمن');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $x = comment::find($id);
        return view('Admin.comment.show',['data' => $x]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function active($id){
        $comment = comment::find($id);
        $comment->status = 'active';
        $comment->update();
        return back();
    }
    
    public function disactive($id){
        $comment = comment::find($id);
        $comment->status = 'desactive';
        $comment->update();
        return back();
    }

    public function read($id){
        $comment = comment::find($id);
        $comment->type = 'watched';
        $comment->update();
        return back();
    }

    public function unread($id){
        $comment = comment::find($id);
        $comment->type = 'unwatched';
        $comment->update();
        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = comment::find($id);
        $comment->delete();
        return back()->with('success','تمت ازالة تعليق');

    }
}
