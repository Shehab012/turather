<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Session;
use Carbon\Carbon;

class searchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Session::get('lang') == 'ar') {
            App::setLocale('ar');
            Carbon::setLocale('ar');
        } elseif(Session::get('lang')=='en') {
            App::setLocale('en');
            Carbon::setLocale('en');
        }

        $res = $request->search;

        $main_cat = \App\main_cat::where('status','1')->get();
        $setting = \App\setting::all();

            
            $items = \App\item::where('title_ar', 'LIKE' ,'%'. $res .'%')
            ->orWhere('title_en','LIKE','%'. $res .'%')
            ->orWhere('desc_ar_mini','LIKE','%'. $res .'%')
            ->orWhere('desc_en_mini','LIKE','%'. $res .'%')
            ->orWhere('seo_ar','LIKE','%'. $res .'%')
            ->orWhere('seo_en','LIKE','%'. $res .'%')->get();
            
            return view('search',['data' => $main_cat , 'items'=>$items, 'settings' => $setting]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
