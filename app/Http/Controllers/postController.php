<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\item;
use App;
use Session;
use Carbon\carbon;

class postController extends Controller
{
    public function index($slogen){
        
        if (Session::get('lang') == 'ar') {
            App::setLocale('ar');
            Carbon::setLocale('ar');
        } elseif(Session::get('lang')=='en') {
            App::setLocale('en');
            Carbon::setLocale('en');
        }

        $main_cat = \App\main_cat::all();
        $setting = \App\setting::all();
        
        $item = \App\item::where('subcat_slogen_ar',$slogen)->orwhere('subcat_slogen_en',$slogen)->get();        
            
            foreach($item as  $value){
                $id = $value->id;
                $x = item::find($id);
                $viwers = $x->viewers;
                $x->viewers = $viwers + 1 ;
                $x->update();
            }
            return view('post.index',[  'items' => $item , 'data' => $main_cat ,'settings' => $setting ]); 
        }
        
        public function allitem($slogen){
            
                
            if (Session::get('lang') == 'ar') {
                App::setLocale('ar');
                Carbon::setLocale('ar');
            } elseif(Session::get('lang')=='en') {
                App::setLocale('en');
                Carbon::setLocale('en');
            }

            $category_name = \App\sub_cat::where('subcat_slogen_ar',$slogen)->orWhere('subcat_slogen_en',$slogen)->first();
            $main_cat = \App\main_cat::all();
            $setting = \App\setting::all();
            
            $subofsub = \App\subofsub::where('s_s_slogan_ar',$slogen)->orwhere('s_s_slogan_en',$slogen)->first();
            
            $sub_id = $subofsub->id;
                            
            $all_item = \App\item::where('sub_id',$sub_id)->paginate(4);
            
            return view('post.all_item',[ 'name'=> $category_name , 'items' => $all_item , 'data' => $main_cat ,'settings' => $setting ]); 

        }
}
