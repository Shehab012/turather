<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;


class authController extends Controller
{
    public function form(){
        return view('auth.login');
    }

    public function sign_in(Request $request){
        $data = $request->all();
        $rules = [
            'email'   => 'required|email',
            'password'=> 'required|min:8'
        ];
        $messages = [
            'email.required'=>'يجب ادخال الايميل',
            'email.email'=>'يجب ادخال البريد الالكتروني بطريقه صحيحه',
            'password.required'=>'يجب ادخال كلمة المرور',
            'password.min'=>'يجب ان تكون كلمة المرور الاتقل عن ثمانية ارقام'
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            if (!Auth::attempt(['email'=>$request->email , 'password'=>$request->password])) {
                return back()->with('error','You must be check your email or password');
            }
        }
			return redirect('/admin');
    }
}

