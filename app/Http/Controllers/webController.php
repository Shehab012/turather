<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;
use App;
use Carbon\Carbon;

class webController extends Controller
{

    public function switcher($lang) {
        Session::put('lang', $lang);
        return redirect()->back();
    }
    
    public function index()
    {
        if (Session::get('lang') == 'ar') {
            App::setLocale('ar');
            Carbon::setLocale('ar');
        } elseif(Session::get('lang')=='en') {
            App::setLocale('en');
            Carbon::setLocale('en');
        }

        $items = \App\item::where('feature','2')
                            ->where('status','1')->get();
        $main_cat = \App\main_cat::where('status','1')->get();
        $setting = \App\setting::all();
        return view('index',[ 'data' => $main_cat , 'item' => $items , 'settings' => $setting ]);        
    }

    public function all_sub_cat($slogen){
        $main_cat = \App\main_cat::all();      
        $setting = \App\setting::all();        
        $main = \App\main_cat::where('subcat_slogen_ar',$slogen)->orwhere('subcat_slogen_en',$slogen)->get();

        return view('category.index',['data' => $main_cat , 'maim' => $main , 'settings' => $setting]);
    }

    public function about(){
        if (Session::get('lang') == 'ar') {
            App::setLocale('ar');
            Carbon::setLocale('ar');
        } elseif(Session::get('lang')=='en') {
            App::setLocale('en');
            Carbon::setLocale('en');
        }

        $main_cat = \App\main_cat::all();
        $setting = \App\setting::all();
        $about = \App\about_us::first();
        return view('about',['data' => $main_cat , 'about'=> $about, 'settings' => $setting]);
    }

    public function contact(){
        if (Session::get('lang') == 'ar') {
            App::setLocale('ar');
            Carbon::setLocale('ar');
        } elseif(Session::get('lang')=='en') {
            App::setLocale('en');
            Carbon::setLocale('en');
        }

        $main_cat = \App\main_cat::all();
        $setting = \App\setting::all();
        return view('contact',['data' => $main_cat , 'settings' => $setting]);
    }

    

    // public function all_post($slogan){
    //     $main_cat = \App\main_cat::all();      
    //     $setting = \App\setting::all();        
    //     $item = \App\item::where('subcat_slogen_ar',$slogen)->orwhere('subcat_slogen_en',$slogen)->get();
    //     $i_id = $item->id;
        
    //     $item->id = $i_id;
    //     $item->viewers = +1;
    //     $item->update();

    //     return view('items.one',['data' => $main_cat , 'items' => $item , 'settings' => $setting]);
        
    // }
}

