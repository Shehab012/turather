<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\subofsub;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;


class subOfSubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub = subofsub::paginate(10);
        return view('Admin.subofsub.index',['data'=>$sub]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sub = \App\sub_cat::all();
        return view('Admin.subofsub.add',[ 'data' => $sub ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'name_ar'     =>'required',
            'name_en'     =>'required',
        ];
        $messages = [
            'name_ar.required'=>'يجب ادخال اسم القسم الفرعي ',
            'name_en.required'=>'يجب ادخال اسم القسم الفرعي ',
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {
            $x = new subofsub;
            $x->sub_cat_id = $request->sub_cat_id;
            $x->sub_of_sub_name_ar = $request->name_ar;
            $x->sub_of_sub_name_en = $request->name_en;
            $x->desc_ar = $request->desc_ar;
            $x->desc_en = $request->desc_en;
            $x->seo_ar = $request->seo_ar;
            $x->seo_en = $request->seo_en;
            $x->status = $request->status;
            $x->s_s_slogan_ar = $this->make_slug($request->input('name_ar'));
            $x->s_s_slogan_en = $this->make_slug($request->input('name_en'));

            $x->save();
            return back()->with('success','تمت اضافة قسم فرعي بنجاح');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $s = subofsub::find($id);
        $sub = \App\sub_cat::all(); 
        return view('Admin.subofsub.edit',['data'=> $s , 'main'=>$sub ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $x = subofsub::find($id);
        $data = $request->all();
        $rules = [
            'name_ar'     =>'required',
            'name_en'     =>'required',
        ];
        $messages = [
            'name_ar.required'=>'يجب ادخال اسم القسم الفرعي ',
            'name_en.required'=>'يجب ادخال اسم القسم الفرعي ',
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x->sub_cat_id = $request->sub_cat_id;
            $x->sub_of_sub_name_ar = $request->name_ar;
            $x->sub_of_sub_name_en = $request->name_en;
            $x->desc_ar = $request->desc_ar;
            $x->desc_en = $request->desc_en;
            $x->seo_ar = $request->seo_ar;
            $x->seo_en = $request->seo_en;
            $x->status = $request->status;
            $x->s_s_slogan_ar = $this->make_slug($request->input('name_ar'));
            $x->s_s_slogan_en = $this->make_slug($request->input('name_en'));

            $x->update();
            return back()->with('success','تمت تعديل فرع فرعي بنجاح');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $x = subofsub::find($id);
        $x->delete();
        return back()->with('error','تم حذف فرع فرعي');
    }
}
