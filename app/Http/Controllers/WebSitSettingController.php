<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\setting;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;


class WebSitSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $x = setting::first();
        return view('Admin.setting',['setting' => $x]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            // 'website_about_ar'=>'required|min:20|max:200',
            // 'website_about_en'=>'required|min:20|max:200',
        ];
        $messages = [
            //  'website_about_ar.min'=>'يجب ان يزيد النص عن 99 حرف',
            //  'website_about_ar.max'=>'يجب ان لا يزيد النص عن 200 حرف',
            //  'website_about_en.min'=>'يجب ان يزيد النص عن 99 حرف',
            //  'website_about_en.max'=>'يجب ان لا يزيد النص عن 200 حرف',
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {
        
            $x = new setting;
            if ($request->hasfile('website_logo_ar')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_logo_ar')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_logo_ar')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_web_ar= $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_logo_ar')->move($public_path, $fileNameToStore_web_ar);
                    
                            $x->website_logo_ar   = $fileNameToStore_web_ar;
    
    
            }
    
            if ($request->hasfile('website_logo_en')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_logo_en')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_logo_en')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_web_en= $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_logo_en')->move($public_path, $fileNameToStore_web_en);
                    
                    $x->website_logo_en   = $fileNameToStore_web_en;
    
    
            }
    
            if ($request->hasfile('website_icon')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_icon')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_icon')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_icon = $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_icon')->move($public_path, $fileNameToStore_icon);
    
            $x->website_icon  = $fileNameToStore_icon;
    
    
            }
    
            if ($request->hasfile('website_logo_footer_ar')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_logo_footer_ar')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_logo_footer_ar')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_logo_footer_ar = $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_logo_footer_ar')->move($public_path, $fileNameToStore_logo_footer_ar);
                    
                            $x->website_logo_footer_ar  = $fileNameToStore_logo_footer_ar;
    
    
            }
    
            if ($request->hasfile('website_logo_footer_en')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_logo_footer_en')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_logo_footer_en')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_logo_footer_en = $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_logo_footer_en')->move($public_path, $fileNameToStore_logo_footer_en);
                    
                $x->website_logo_footer_en  = $fileNameToStore_logo_footer_en;
    
    
            }
    
    
            
    
    
    
            $x->website_name_ar                   = $request->website_name_ar;
            $x->website_name_en                   = $request->website_name_en;
            $x->website_address_ar                = $request->website_address_ar;
            $x->website_address_en                = $request->website_address_en;
            $x->website_about_ar                  = $request->website_about_ar;
            $x->website_about_en                  = $request->website_about_en;
            $x->website_email                     = $request->website_email;
            $x->website_phone                     = $request->website_phone;
    
            $x->website_keywor_ar                 = $request->website_keywor_ar;
            $x->website_keywor_en                 = $request->website_keywor_en;
            $x->website_desc_ar                   = $request->website_desc_ar;
            $x->website_desc_en                   = $request->website_desc_en;
            $x->website_facebook                  = $request->website_facebook;
            $x->website_twitter                   = $request->website_twitter;
            $x->website_google                    = $request->website_google;
            $x->website_youtube                   = $request->website_youtube;
            $x->website_linked_in                 = $request->website_linked_in;
            $x->website_instgram                  = $request->website_instgram;
            $x->save();
            return back()->with('success','تمت عمليه الاضافه بنجاح');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $rules = [
            // 'website_about_ar'=>'required|min:20|max:200',
            // 'website_about_en'=>'required|min:20|max:200',
        ];
        $messages = [
        //      'website_about_ar.min'=>'يجب ان يزيد النص عن 99 حرف',
        //      'website_about_ar.max'=>'يجب ان لا يزيد النص عن 200 حرف',
        //      'website_about_en.min'=>'يجب ان يزيد النص عن 99 حرف',
        //      'website_about_en.max'=>'يجب ان لا يزيد النص عن 200 حرف',
         ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            
            $x =  setting::find($id);
            
            if ($request->hasfile('website_logo_ar')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_logo_ar')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_logo_ar')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_web_ar= $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_logo_ar')->move($public_path, $fileNameToStore_web_ar);
    
                    $x->website_logo_ar = $fileNameToStore_web_ar;
                    
            }
    
            if ($request->hasfile('website_logo_en')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_logo_en')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_logo_en')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_web_en= $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_logo_en')->move($public_path, $fileNameToStore_web_en);
    
                    $x->website_logo_en = $fileNameToStore_web_en;                
    
            }
    
            if ($request->hasfile('website_icon')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_icon')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_icon')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_icon = $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_icon')->move($public_path, $fileNameToStore_icon);
    
                    $x->website_icon = $fileNameToStore_icon;                
            }
    
            if ($request->hasfile('website_logo_footer_ar')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_logo_footer_ar')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_logo_footer_ar')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_logo_footer_ar = $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_logo_footer_ar')->move($public_path, $fileNameToStore_logo_footer_ar);
    
                $x->website_logo_footer_ar = $fileNameToStore_logo_footer_ar;                
    
            }
    
            if ($request->hasfile('website_logo_footer_en')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('website_logo_footer_en')->getClientOriginalName();
    
                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);
    
                // get file ext
                $extension = $request->file('website_logo_footer_en')->getClientOriginalExtension();
    
                // file name to store
                $fileNameToStore_logo_footer_en = $filename.'_'.time().'.'.$extension;
    
                // upload image
                    $public_path = public_path('/webImage');
                    $path = $request->file('website_logo_footer_en')->move($public_path, $fileNameToStore_logo_footer_en);
    
                $x->website_logo_footer_en  = $fileNameToStore_logo_footer_en;                
    
            }
    
            $x->website_name_ar                   = $request->website_name_ar;
            $x->website_name_en                   = $request->website_name_en;
            $x->website_address_ar                = $request->website_address_ar;
            $x->website_address_en                = $request->website_address_en;
            $x->website_about_ar                  = $request->website_about_ar;
            $x->website_about_en                  = $request->website_about_en;
            $x->website_email                     = $request->website_email;
            $x->website_phone                     = $request->website_phone;
            // End images 
            $x->website_keywor_ar                 = $request->website_keywor_ar;
            $x->website_keywor_en                 = $request->website_keywor_en;
            $x->website_desc_ar                   = $request->website_desc_ar;
            $x->website_desc_en                   = $request->website_desc_en;
            $x->website_facebook                  = $request->website_facebook;
            $x->website_twitter                   = $request->website_twitter;
            $x->website_google                    = $request->website_google;
            $x->website_youtube                   = $request->website_youtube;
            $x->website_linked_in                 = $request->website_linked_in;
            $x->website_instgram                  = $request->website_instgram;
            $x->update();
            return back()->with('success','تم التعديل بنجاح');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
