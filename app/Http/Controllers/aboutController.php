<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\about_us;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;


class aboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $about = about::all()->limit('1');        
        // return view('admin.about.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.about.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'title_ar' =>'required',
            'title_en' =>'required',
            'desc_ar'=>'required',
            'desc_en'=>'required',
            'Image'=>'required',
        ];
        $messages = [
            'title_ar.required'=>'يجب ادخال الاسم   ',
            'title_en.required'=>'يجب ادخال الاسم  ',
            'desc_ar.required'=>'يجب ادخال التعليق  ',
            'desc_en.required'=>'يجب ادخال التعليق  ',
            'Image.required'=>'يجب ادخال صوره  ',
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            if ($request->hasfile('Image')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('Image')->getClientOriginalName();

                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);

                // get file ext
                $extension = $request->file('Image')->getClientOriginalExtension();

                // file name to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;

                // upload image
                $public_path = public_path('/webImage');
                $path = $request->file('Image')->move($public_path, $fileNameToStore);


            }else{

                    $fileNameToStore = 'noimage.png';
            }

            $x = new about_us;
            $x->title_ar = $request->title_ar;
            $x->title_en = $request->title_en;
            $x->desc_ar  = $request->desc_ar;
            $x->desc_en  = $request->desc_en;
            $x->image    = $fileNameToStore;
            $x->save();
            return back()->with('success','تم اضافة معلومات صفحه من نحن');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $rules = [
            'title_ar' =>'required',
            'title_en' =>'required',
            'desc_ar'=>'required',
            'desc_en'=>'required',
            'Image'=>'required',
        ];
        $messages = [
            'title_ar.required'=>'يجب ادخال الاسم   ',
            'title_en.required'=>'يجب ادخال الاسم  ',
            'desc_ar.required'=>'يجب ادخال التعليق  ',
            'desc_en.required'=>'يجب ادخال التعليق  ',
            'Image.required'=>'يجب ادخال صوره  ',
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {
            
            $x =  about_us::find($id);

            if ($request->hasfile('Image')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('Image')->getClientOriginalName();

                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);

                // get file ext
                $extension = $request->file('Image')->getClientOriginalExtension();

                // file name to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;

                // upload image
                $public_path = public_path('/webImage');
                $path = $request->file('Image')->move($public_path, $fileNameToStore);
                
                $x->image    = $fileNameToStore;


            }
            
            $x->title_ar = $request->title_ar;
            $x->title_en = $request->title_en;
            $x->desc_ar  = $request->desc_ar;
            $x->desc_en  = $request->desc_en;
            $x->update();
            return back()->with('success','تم تعديل معلومات صفحه من نحن');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
