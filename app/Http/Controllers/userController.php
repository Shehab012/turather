<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $x = User::paginate(10);        
        return view('Admin.user.index',['data' => $x]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'name' => 'required|min:4',
            'email'=>'email',
            'password' => 'required|min:8|confirmed',
        ];
        $messages = [
            'name.required' => 'يجب ادخال اسم للمستخدم',
            'name.min' => 'يجب الايقل عن اربع حروف',
            'email.email' => 'خطئ ف ادخال البريد الالكتروني',
            'password.required' => 'يجب ادخال كلمة المرور',
            // 'password.max'=>'يجب الايزيد عن ثمانيه ارقام  او احرف ',
            'password.min'=>'يجب الايقل عن ثمانيه احرف',
            'password.confirmed'=>'كلمه المرور غير مطابقه'            
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {
            
        $x = new User;
        $x->name  = $request->name;
        $x->email = $request->email;
        $x->password = bcrypt($request->password);
        $x->save();
        return back()->with('success','تمت اضافه مستخدم بنجاح');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $x = User::find($id);
        return view('Admin.user.edit',['data'=>$x]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        $rules = [
            'name' => 'required|min:4',
            'email'=>'email',
            'password' => 'required|min:8|confirmed',
        ];
        $messages = [
            'name.required' => 'يجب ادخال اسم للمستخدم',
            'name.min' => 'يجب الايقل عن اربع حروف',
            'email.email' => 'خطئ ف ادخال البريد الالكتروني',
            'password.required' => 'يجب ادخال كلمة المرور',
            // 'password.max'=>'يجب الايزيد عن ثمانيه ارقام  او احرف ',
            'password.min'=>'يجب الايقل عن ثمانيه احرف',
            'password.confirmed'=>'كلمه المرور غير مطابقه'             
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x =  User::find($id);
            $x->id = $id;
            $x->name = $request->name;
            $x->email = $request->email;
            $x->password = bcrypt($request->password);
            $x->update();
            return back()->with('success','تم تعديل مستخدم بنجاح');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $x = User::find($id);
        $x->delete();
        return back()->with('error','تمت ازله مستخدم');
    }
}
