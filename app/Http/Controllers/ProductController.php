<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App;
use Carbon\Carbon;

class ProductController extends Controller
{
    public function index(){
        if(Session::get('lang') == ''){
            Session::get('lang') == 'ar';
        }
        if (Session::get('lang') == 'ar') {
            App::setLocale('ar');
            Carbon::setLocale('ar');
        } elseif(Session::get('lang')=='en') {
            App::setLocale('en');
            Carbon::setLocale('en');
        }

        $main_cat = \App\main_cat::where('status','1')->get();
        $setting = \App\setting::all();
        $products = \App\product::paginate(6);

        return view('product.index',[ 'products'=>$products, 'settings' => $setting , 'data' => $main_cat ]);
    }

    public function product_details($slogan){
        if(Session::get('lang') == ''){
            Session::get('lang') == 'ar';
        }
        if (Session::get('lang') == 'ar') {
            App::setLocale('ar');
            Carbon::setLocale('ar');
        } elseif(Session::get('lang')=='en') {
            App::setLocale('en');
            Carbon::setLocale('en');
        }
        $main_cat = \App\main_cat::where('status','1')->get();
        $setting = \App\setting::all();
        $product = \App\product::where('product_slogan_ar',$slogan)->orWhere('product_slogan_en',$slogan)->first();
        
        $viewers = $product->viewers;
        $product->viewers = $viewers + 1;
        $product->update();

        return view('product.details',[ 'product'=>$product, 'settings' => $setting , 'data' => $main_cat ]);
    }

    public function admin_index(){
        $products = \App\product::paginate(10);
        return view('/Admin.product.index',['products'=>$products]);
    }

    public function create(){
        return view('Admin.product.add');
    }

    public function store(request $request){
        $data = $request->all();
        $rules = [
            'product_name_ar'=> 'required',
            'product_name_en'=> 'required',
            'desc_ar'=> 'required',
            'desc_en'=> 'required',

            'price'=>'required|numeric',
            'small_ar_desc'=>'required',
            'small_en_desc'=>'required',

            'Image'=> 'required|image|mimes:jpeg,jpg,png',
        ];
        $messages = [
            'product_name_ar.required'=>'يجب ادخال اسم المنتج بالعربي',
            'product_name_en.required'=>'يجب ادخال اسم المنتج بالانجليزيه',

            'desc_ar.required'=>'يجب ادخال الوصف بالعربي',
            'desc_en.required'=>'يجب ادخال الوصف بالانجليزيه',

            'order.required'=>'يجب تحديد الترتيب',
            'order.integer'=>'يجب ان يكون الترتيب رقم',

            'Image.required'=>'يجب اخيار صوره',
            'Image.mimes'=>'يجب ان يكون المسار jpg او png',
            'Image.image'=>'يجب ان يكون المسار jpg او png',

            'price.required'=>'يجب تحديد سعر للمنتج',
            'price.numeric'=>'يجب تحديد سعر بالرقم',
            
            'small_ar_desc.required'=>'يجب ادخال الوصف المصغر',
            'small_en_desc.required'=>'يجب ادخال الوصف المصغر',
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            if ($request->hasfile('Image')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('Image')->getClientOriginalName();

                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);

                // get file ext
                $extension = $request->file('Image')->getClientOriginalExtension();

                // file name to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;

                // upload image
                $public_path = public_path('/webImage');
                $path = $request->file('Image')->move($public_path, $fileNameToStore);

                }
                $x = new \App\product;
                $x->product_title_ar = $request->product_name_ar;
                $x->product_title_en = $request->product_name_en;
                $x->price = $request->price;
                $x->image = $fileNameToStore;
                $x->product_order = $request->order;
                $x->product_type = $request->type;
                $x->product_desc_ar = $request->desc_ar;
                $x->product_desc_en = $request->desc_en;
                $x->product_small_desc_ar = $request->small_ar_desc;
                $x->product_small_desc_en = $request->small_en_desc;

                $x->product_slogan_ar = $this->make_slug($request->input('product_name_ar'));
                $x->product_slogan_en = $this->make_slug($request->input('product_name_en'));
    

                $x->save();
                return back()->with('success','تمت اضافة منتج');
            }
        }

    public function edit($id){
        $products = \App\product::find($id);
        return view('Admin.product.edit',['data'=>$products]);
    }

    public function update(Request $request ,$id){
        $data = $request->all();
        $rules = [
            'product_name_ar'=> 'required',
            'product_name_en'=> 'required',
            'desc_ar'=> 'required',
            'desc_en'=> 'required',

            'price'=>'required|numeric',
            'small_ar_desc'=>'required',
            'small_en_desc'=>'required',

        ];
        $messages = [
            'product_name_ar.required'=>'يجب ادخال اسم المنتج بالعربي',
            'product_name_en.required'=>'يجب ادخال اسم المنتج بالانجليزيه',

            'desc_ar.required'=>'يجب ادخال الوصف بالعربي',
            'desc_en.required'=>'يجب ادخال الوصف بالانجليزيه',


            'price.required'=>'يجب تحديد سعر للمنتج',
            'price.numeric'=>'يجب تحديد سعر بالرقم',

            'small_ar_desc.required'=>'يجب ادخال الوصف المصغر',
            'small_en_desc.required'=>'يجب ادخال الوصف المصغر',
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x =  \App\product::find($id);            

            if ($request->hasfile('Image')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('Image')->getClientOriginalName();

                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);

                // get file ext
                $extension = $request->file('Image')->getClientOriginalExtension();

                // file name to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;

                // upload image
                $public_path = public_path('/webImage');
                $path = $request->file('Image')->move($public_path, $fileNameToStore);
                $x->image = $fileNameToStore;                
                }

                $x->product_title_ar = $request->product_name_ar;
                $x->product_title_en = $request->product_name_en;
                $x->price = $request->price;
                $x->product_order = $request->order;
                $x->product_type = $request->type;
                $x->product_desc_ar = $request->desc_ar;
                $x->product_desc_en = $request->desc_en;
                $x->product_small_desc_ar = $request->small_ar_desc;
                $x->product_small_desc_en = $request->small_en_desc;

                $x->product_slogan_ar = $this->make_slug($request->input('product_name_ar'));
                $x->product_slogan_en = $this->make_slug($request->input('product_name_en'));
    

                $x->update();
                return back()->with('success','تمت تعديل منتج');
            }
    }

    public function destroy($id){
        $x = \App\product::find($id);
        $x->delete();
        return back()->with('error','تمت ازاله المنتج');
    }
}
