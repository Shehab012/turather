<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\sub_cat;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;


class sub_categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $x =  sub_cat::paginate(10);
        return view('Admin.sub_category.index',['data' => $x]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $x = \App\main_cat::all();
        return view('Admin.sub_category.add',['data'=>$x]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'name_ar'     =>'required',
            'name_en'     =>'required',
            // 'desc_ar'     =>'required',
            // 'desc_en'     =>'required',
            'order'       =>'required|integer|unique:sub_cat,order',
            'main_cat_id' =>'required',
            'status'      =>'required',
            // 'seo_ar'      =>'required',
            // 'seo_en'      =>'required'
        ];
        $messages = [
            'name_ar.required'=>'يجب ادخال اسم القسم الفرعي ',
            'name_en.required'=>'يجب ادخال اسم القسم الفرعي ',
            // 'desc_ar.required'=>'يجب ادخال وصف للقسم',
            // 'desc_en.required'=>'يجب ادخال وصف للقسم',
            'order.required'=>'يجب اختيار ترتيب القسم',
            'order.integer'=>' يجب ان يكون  ترتيب القسم رقم',
            'order.unique'=>'لقد تم اختيار هذا الترتيب من قبل',
            'main_cat_id.required'=>'يجب ادخال  القسم الرائيسي',
            'status.required'=>'يجب اختيار التفعيل',
            // 'seo_ar.required'=>'يجب ادخال الكلمات المفتاحيه',
            // 'seo_en.required'=>'يجب ادخال الكلمات المفتاحيه'
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x = new sub_cat;
            $x->sub_cat_name_ar = $request->name_ar;
            $x->sub_cat_name_en = $request->name_en;
            $x->cat_desc_ar = $request->desc_ar;
            $x->cat_desc_en = $request->desc_en;
            $x->order       = $request->order;
            $x->main_cat_id = $request->main_cat_id;
            $x->status      = $request->status;
            $x->seo_ar      = $request->seo_ar; 
            $x->seo_en      = $request->seo_en;
            $x->subcat_slogen_ar = $this->make_slug($request->input('name_ar'));
            $x->subcat_slogen_en = $this->make_slug($request->input('name_en'));
            $x->save();
            return back()->with('success','تمت اضافه فرع رئيسي بنجاح');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $x = sub_cat::find($id);
        $main = \App\main_cat::all();
        return view('Admin.sub_category.edit',['data'=>$x,'main'=>$main]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $x = sub_cat::find($id);        
        $data = $request->all();
        $rules = [
            'name_ar'     =>'required',
            'name_en'     =>'required',
            // 'desc_ar'     =>'required',
            // 'desc_en'     =>'required',
            'order'       =>'required|integer',
            'main_cat_id' =>'required',
            'status'      =>'required',
            // 'seo_ar'      =>'required',
            // 'seo_en'      =>'required'
        ];
        $messages = [
            'name_ar.required'=>'يجب ادخال اسم القسم الفرعي ',
            'name_en.required'=>'يجب ادخال اسم القسم الفرعي ',
            // 'desc_ar.required'=>'يجب ادخال وصف للقسم',
            // 'desc_en.required'=>'يجب ادخال وصف للقسم',
            'order.required'=>'يجب اختيار ترتيب القسم',
            'order.integer'=>' يجب ان يكون  ترتيب القسم رقم',
            // 'order.unique'=>'لقد تم اختيار هذا الترتيب من قبل',
            'main_cat_id.required'=>'يجب ادخال  القسم الرائيسي',
            'status.required'=>'يجب اختيار التفعيل',
            // 'seo_ar.required'=>'يجب ادخال الكلمات المفتاحيه',
            // 'seo_en.required'=>'يجب ادخال الكلمات المفتاحيه'
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x->id = $id;
            $x->sub_cat_name_ar = $request->name_ar; 
            $x->sub_cat_name_en = $request->name_en;
            $x->cat_desc_ar     = $request->desc_ar;
            $x->cat_desc_en     = $request->desc_en;
            $x->seo_ar          = $request->seo_ar;
            $x->seo_en          = $request->seo_en;
            $x->order           = $request->order;
            $x->status          = $request->status;
            $x->main_cat_id     = $request->main_cat_id;
            $x->subcat_slogen_ar = $this->make_slug($request->input('name_ar'));
            $x->subcat_slogen_en = $this->make_slug($request->input('name_en'));
            $x->update();
            return back()->with('success','تم تعديل فرع رئيسي بنجاح');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $x = sub_cat::find($id);
        $x->delete();
        return back()->with('error','تمت ازاله فرع');
    }
}
