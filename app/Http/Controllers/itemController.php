<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\item;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class itemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = item::paginate(10);
        return view('Admin.items.index',['data'=>$item]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        $s_category = \App\sub_cat::all();
            $sub = \App\subofsub::where('sub_cat_id',$request->id)->get();
            if ($request->ajax()) {
                return response()->json($request->id);           
            }
            // return response()->json($ids);
            return view('Admin.items.add',['ids'=>$sub ,'sub'=>$s_category])->render();            
    }

    public function OpenOpen(Request $request) {
        if ($request->ajax()) {
            $subcat = DB::table('subofsub')->where('sub_cat_id', $request->sub_cat_id)->get();

            $data = view('Admin.items.ajax-open', compact('subcat'))->render();
            return response()->json(['options' => $data]);
        }
    }
    
    public function create_ajax( ){
        // var_dump(request()->ajax());
        if(request()->ajax()){
            return view('Admin.items.add',compact('ids'));
        };


        // return response()->json(['msg'=>$validator->errors()->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        $rules = [
            'title_ar'=> 'required',
            'title_en'=> 'required',
            'desc_sm_ar'=>'required',
            'desc_sm_en'=>'required',
            // 'seo_ar'=> 'required',
            // 'seo_en'=> 'required',
            'desc_ar'=> 'required',
            'desc_en'=> 'required',
            'feu'=> 'required',
            // 'status'=> 'required',
            'Image'=> 'required|image|mimes:jpeg,jpg,png',
            'sub_cat_id'=> 'required',
            'sub_id'=>'required'
        ];
        $messages = [
            'title_ar.required'=>'يجب ادخال اسم المنتج بالعربي',
            'title_en.required'=>'يجب ادخال اسم المنتج بالانجليزيه',
             'desc_sm_ar.min'=>'يجب ان يزيد النص عن 99 حرف',
             'desc_sm_en.min'=>'يجب ان لا يزيد النص عن 200 حرف',
            'desc_ar.required'=>'يجب ادخال الوصف بالعربي',
            'desc_en.required'=>'يجب ادخال الوصف بالانجليزيه',
            // 'seo_ar.required'=>'يجب ادخال كلمات المتاحه',
            // 'seo_en.required'=>'يجب ادخال كلمات المتاحه',
            'feu.required'=>'يجب تحديد الاهميه',
            'Image.required'=>'يجب اخيار صوره',
            'Image.mimes'=>'يجب ان يكون المسار jpg او png',
            'Image.image'=>'يجب ان يكون المسار jpg او png',
            'sub_cat_id.required'=>'يجب اختيار فرع رائيسي',
            'sub.required'=>'يجب اختيار فرع فرعي'
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            if ($request->hasfile('Image')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('Image')->getClientOriginalName();

                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);

                // get file ext
                $extension = $request->file('Image')->getClientOriginalExtension();

                // file name to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;

                // upload image
                $public_path = public_path('/webImage');
                $path = $request->file('Image')->move($public_path, $fileNameToStore);


            }else{

                    $fileNameToStore = 'noimage.png';
            }
            
            $x = new item;
            $x->sub_cat_id       =  $request->sub_cat_id;
            $x->sub_id           =  $request->sub_id;

            $x->title_ar         =  $request->title_ar;
            $x->title_en         =  $request->title_en;
            $x->desc_ar          =  $request->desc_ar;
            $x->desc_en          =  $request->desc_en;
            $x->seo_ar           =  $request->seo_ar;
            $x->seo_en           =  $request->seo_en;
            $x->desc_ar_mini     =  $request->desc_sm_ar;
            $x->desc_en_mini     =  $request->desc_sm_en;
            $x->image            =  $fileNameToStore;
            $x->feature          =  $request->feu;
            $x->status           =  $request->status;
            $x->subcat_slogen_ar = $this->make_slug($request->input('title_ar'));
            $x->subcat_slogen_en = $this->make_slug($request->input('title_en'));
            $x->save();
            return back()->with('success','تمت اضافه المقال بنجاح');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $x = item::find($id);
        $s_category = \App\sub_cat::all();
        return view('Admin.items.edit',['data'=>$x,'sub'=>$s_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = $request->all();
        $rules = [
            'title_ar'=> 'required',
            'title_en'=> 'required',
            'desc_sm_ar'=>'required',
            'desc_sm_en'=>'required',
            // 'seo_ar'=> 'required',
            // 'seo_en'=> 'required',
            'desc_ar'=> 'required',
            'desc_en'=> 'required',
            'feu'=> 'required',
            // 'status'=> 'required',
            // 'Image'=> 'image|mimes:jpeg,jpg,png',
            'sub_cat_id'=> 'required',
            'sub_id'=>'required'
        ];
        $messages = [
            'title_ar.required'=>'يجب ادخال اسم المنتج بالعربي',
            'title_en.required'=>'يجب ادخال اسم المنتج بالانجليزيه',
             'desc_sm_ar.min'=>'يجب ان يزيد النص عن 99 حرف',
             'desc_sm_en.min'=>'يجب ان لا يزيد النص عن 200 حرف',
            'desc_ar.required'=>'يجب ادخال الوصف بالعربي',
            'desc_en.required'=>'يجب ادخال الوصف بالانجليزيه',
            // 'seo_ar.required'=>'يجب ادخال كلمات المتاحه',
            // 'seo_en.required'=>'يجب ادخال كلمات المتاحه',
            'feu.required'=>'يجب تحديد الاهميه',
            // 'Image.image'=>'يجب ان يكون المسار jpg او png',
            'sub_cat_id.required'=>'يجب اختيار فرع رائيسي',
            'sub.required'=>'يجب اختيار فرع فرعي'
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x = item::find($id);            

            if ($request->hasfile('Image')) {
                
                // get file name with ext
                $filenameWithExt = $request->file('Image')->getClientOriginalName();

                // get just fill name
                $filename = pathinfo($filenameWithExt , PATHINFO_FILENAME);

                // get file ext
                $extension = $request->file('Image')->getClientOriginalExtension();

                // file name to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;

                // upload image
                $public_path = public_path('/webImage');
                $path = $request->file('Image')->move($public_path, $fileNameToStore);
                    
                    $x->image =  $fileNameToStore;


            }

            $x->id = $id;
            $x->sub_cat_id       =  $request->sub_cat_id;
            $x->sub_id           =  $request->sub_id;

            $x->title_ar    =  $request->title_ar;
            $x->title_en    =  $request->title_en;
            $x->desc_ar     =  $request->desc_ar;
            $x->desc_en     =  $request->desc_en;
            $x->seo_ar      =  $request->seo_ar;
            $x->seo_en      =  $request->seo_en;
            $x->feature     =  $request->feu;
            $x->status      =  $request->status;
            $x->subcat_slogen_ar = $this->make_slug($request->input('title_ar'));
            $x->subcat_slogen_en = $this->make_slug($request->input('title_en'));
            $x->update();
            return back()->with('success','تم تعديل المقال بنجاح');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $i = item::find($id);
        $i->delete();
        return back()->with('error','تمت ازاله مستخدم');
    }
}
