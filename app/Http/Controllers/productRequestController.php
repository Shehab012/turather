<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\order;

class productRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = order::paginate(10);
        return view('Admin.order.index',['orders'=>$order]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = order::find($id);
        $order->status = 2;
        $order->update(); 
        return view('Admin.order.show',['data'=>$order]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'fname'=> 'required',
            'lname'=> 'required',
            'email'=> 'required|email',
            'phone'=> 'required',
            'note'=>'required'

        ];
        $messages = [
            'fname.required'=>'يجب ادخال الاسم الاول',
            'lname.required'=>'يجب ادخال الاسم الاخير',
            'email.required'=>'يجب ادخال ايميل',
            'email.email'=>'يجب ان تكون صيغه ايميل صحيحه',
            'phone.required'=>'يجب ادخال رقم للتواصل',
            'note.required'=>'يجب ادخال نص ',
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x = new order;
            $x->product_id = $request->product_id;
            $x->first_name = $request->fname;
            $x->last_name = $request->lname;
            $x->email = $request->email;
            $x->phone = $request->phone;
            $x->note = $request->note;
            $x->save();
            return back()->with('success','تم ارسال الطلب للادمن و سوف يتم التواص معك');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = order::find($id);
        $order->delete();
        return back()->with('error','تمت ازلة الطلب');       
    }
}
