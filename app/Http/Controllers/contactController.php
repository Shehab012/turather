<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\contact;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;


class contactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contact = contact::paginate(10);
        return view('Admin.contact.index',['data' => $contact]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $rules = [
            'fname'  => 'required',
            'lname'  => 'required',
            'email'  => 'required|email',
            'address'=> 'required',
            'massege'=> 'required',
        ];
        $messages = [
            'fname.required'=>'يجب ادخال الاسم الاول',
            'lname.required'=>'يجب ادخال الاسم الثاني',
            'email.required'=>'يجب ادخال البريد الالكتروني',
            'email.email'=>' يجب ادخال البريد الالكتروني بطريقه صحيحه',
            'address.required'=>'يجب ادخال العنوان',
            'massege.required'=>'يجب ادخال الرساله'
            
        ];
        $Validator = Validator::make($data, $rules, $messages);
        if ($Validator->fails()) {
            return redirect()->back()->withErrors($Validator)->withInput(Input::all());
        } else {

            $x = new contact;
            $x->fname   = $request->fname;
            $x->lname   = $request->lname;
            $x->email   = $request->email;
            $x->address = $request->address;
            $x->massege = $request->massege;
            $x->save();
            return back()->with('success','تم الارسال بنجاح');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = contact::find($id);
        return view('Admin.contact.show',['data'=>$contact]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = contact::find($id);
        $contact->delete();
        return back()->with('تمت الازاله');
    }
}
