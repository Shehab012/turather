<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class setting extends Model
{
    protected $table = 'settings';

    protected $fileable=[
        'website_namr_ar','website_namr_en','website_address_ar',
        'website_address_en','website_about_ar','website_about_en',
        'website_email','website_phone','website_logo_ar','website_logo_en',
        'website_icon','website_keywor_ar','website_keywor_en','website_desc_ar',
        'website_desc_en','website_facebook','website_twitter','website_google',
        'website_youtuob','website_linked_in','website_instgram'
    ];
}
