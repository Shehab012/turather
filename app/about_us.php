<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class about_us extends Model
{
    protected $table = 'about';

    protected $fileable = [
        'title_ar','title_en',
        'desc_ar','desc_en','image'
    ];
}
