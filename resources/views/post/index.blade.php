@extends('Indexlayout.master')
@section('content')
<section class="sec-pd">
    <div class="container">
        @foreach($items as $item)
        <div class="row">
            <div class="col-md-8">
                <div class="post sub-sec">
                    <ol class="breadcrumb">
                    <li><a href="/"> <i class="fas fa-home"></i> {{ trans('home.home') }}</a></li>
                        </ol>
                <div class="sec-image">
                <img src="{{asset('webImage/'.$item->image)}}" class="img-responsive" />
        
                </div>
                <div class="info-top">
                    <span><i class="far fa-clock"></i>  {{ $item->created_at->format('Y-m-d') }}</span>
                    <span class="pull-left">
                        <a href="#">
                                <?php
                                    $i_comment = \App\comment::where('item_id',$item->id)
                                                              ->where('status','active')->get();
                                ?>
                                @if(count($i_comment) > 0)
                                    <i class="fas fa-comments"> </i>  {{ count($i_comment) }}</a>
                                @endif
        
                    </span>
                </div>
                @if(Session::get('lang')=='ar')
                    <h2>{{ $item->title_ar }}</h2>
                @endif
        
                @if(Session::get('lang')=='en')
                    <h2>{{ $item->title_en }}</h2>
                @endif
        
                @if(Session::get('lang')=='ar')
                    <p> {!! $item->desc_ar !!}</p>
                @endif
        
                @if(Session::get('lang')=='en')
                    <p> {!! $item->desc_en !!}</p>
                @endif
                    <ul class="list-unstyled tags">
                            @if(Session::get('lang')=='ar')
                                <li><span class="sp-title"> كلمات </span>  <a href="#"> {{ $item->seo_ar }} </a></li>
                            @endif                 
        
                            @if(Session::get('lang')=='en')
                                <li><span class="sp-title"> Words </span>  <a href="#"> {{ $item->seo_en }} </a></li>
                            @endif                    
                        </ul>
                        <hr>
                    </div>
                    <?php
                        $item_comment = \App\comment::where('item_id',$item->id)
                                        ->where('status','active')->get();
                    ?>
                    @endforeach
                    
                    @foreach($item_comment as $comment )
                        <div class="post sub-sec profile-info">
                                <div class="tr-submain">
                                        <div class="tr-info">
                                            <h4>
                                            <a href="#">{{ $comment->name }}</a>
                                            </h4>
                                            <div class="info-main">
                                                <span>
                                                    {{ $comment->desc }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <small>{{ $comment->created_at->diffForHumans() }}</small>
                                    </div>
                                </div>
                    @endforeach
        
                    <div class="post sub-sec">
                            <div class="comment">
                                @include('Adminlayout.errors')
                                    <div class="sec-title">
                                            <h3>{{ trans('home.Comments') }}</h3>
                                        </div>
                                        <hr>
                                    <form method="POST" action="/postcomment">
                                        {{ csrf_field() }}
                                           <div class="row">
                                           <div>
                                           <input class="hidden" name="item_id" value="{{ $item->id }}">
                                           </div>
                                                <div class="col-md-12 form-group">
                                                        <textarea name="massege" class="form-control" placeholder="{{ trans('home.TheMessage') }}"></textarea>
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                        <input type="text" name="name" class="form-control" placeholder="{{ trans('home.Name') }}" />
                                                    </div>
                                                    <div class="col-md-6 form-group">
                                                    <input type="email" name="email" class="form-control" placeholder="{{ trans('home.Enteryouremailaddress') }}" />
                                                    </div>
                
                
                                                    <div class="form-group group-btn-tours col-xs-12 ">
                                                    <button type="sumbit" class="btn btn-success">{{ trans('home.Send') }}</button>
                                                    </div>
                                           </div>
                                        </form>
                                </div>
                            </div>
        
            </div>
            @include('Indexlayout.sidebar');
</div>
    </div>
</section>

@endsection