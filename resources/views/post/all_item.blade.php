@extends('Indexlayout.master')
@section('content')
<section class="sec-pd">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                            <div class="post sub-sec">
                                    <ol class="breadcrumb">
                                                <li><a href="/"> <i class="fas fa-home"></i>@lang('home.home')</a></li>
                                            {{-- @if(Session::get('lang') == 'ar')
                                                <li><a href="#">{{ $name->sub_cat_name_ar }}</a></li>
                                            @elseif(Session::get('lang') == 'en')
                                                <li><a href="#"> {{ $name->sub_cat_name_en }}</a></li>
                                            @endif --}}
                                    </ol>
                                <hr>
                                @foreach($items as $item)
                                    <div class="sec-image">
                                        <a href="#"> <img src="{{ asset('webImage/'.$item->image) }}" class="img-responsive" /></a>
                                    </div>
                                    <div class="info-top">
                                        <span>
                                            <i class="far fa-clock"></i>{{ $item->created_at->format('Y-m-d') }}</span>
                                        <span class="pull-left">
                                            <a href="#">
                                            <?php
                                                $i_comment = \App\comment::where('item_id',$item->id)
                                                                  ->where('status','active')->get();
                                            ?>
                                            @if(count($i_comment) > 0)
                                                <i class="fas fa-comments"> </i>  {{ count($i_comment) }}</a>
                                            @endif
                                        </span>
                                    </div>
                                    <div>
                                        @if(Session::get('lang')=='ar')
                                            <h2> <a href="/post/{{ $item->subcat_slogen_ar }}"> {{ $item->title_ar }} </a> </h2>
                                        @elseif(Session::get('lang') == 'en')
                                            <h2> <a href="/post/{{ $item->subcat_slogen_en }}"> {{ $item->title_en }} </a> </h2>
                                        @endif
                                    
                                    @if(Session::get('lang') == 'ar')
                                    <p> {{ $item->desc_ar_mini }} </p>
                                    @elseif(Session::get('lang') == 'en')
                                    <p> {{ $item->desc_en_mini }} </p>
                                    @endif
    
                                    @if(Session::get('lang') == 'ar')
                                        <a href="/post/{{$item->subcat_slogen_ar}}" class="btn btn-success"> @lang('home.ReadMore') <i class="fas fa-angle-double-left"></i></a>
                                    @elseif(Session::get('lang') == 'en')
                                        <a href="/post/{{ $item->subcat_slogen_en }}" class="btn btn-success"> @lang('home.ReadMore') <i class="fas fa-angle-double-left"></i></a>
                                    @endif
                                    <hr>
                                </div>
                                @endforeach
                                {{ $items->links() }}
                        </div>
                    </div>
                        @include('Indexlayout.sidebar');
                </div>
    </div>
</section>
@endsection