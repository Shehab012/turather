
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <img src="{{url("/back/user-laptop-512.png")}}" style="    height: 70px;width: 70px;margin-right: 62px;margin-top: -14px;" alt="user-img" class="img-circle">
                <!--                <li> 
                                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                        <i class="fa fa-user"></i>
                                        <span class="hide-menu">abdelrhman yassein </span>
                                    </a>
                                    <ul aria-expanded="false" class="collapse">
                                        <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Balance</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
                                        <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> Logout</a></li>
                                    </ul>
                                </li>-->

                <li class="nav-small-cap">------------------------------</li>
                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin')}}" aria-expanded="false">
                        <i class="fa fa-home"></i>
                        <span class="hide-menu">الصفحة الرئيسية</span>
                    </a>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="fa fa-window-maximize"></i>
                        <span class="hide-menu">اعدادات الموقع</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/admin/websetting')}}"><i class="fa fa-gear"></i>&nbsp; الاعدادات الرئيسية</a></li>
                    </ul>
                </li>

                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin/maincategory')}}" aria-expanded="false">
                        <i class="fa fa-home"></i>
                        <span class="hide-menu">  الاقسام </span>
                    </a>
                </li>

                
                <li> 
                        <a class="waves-effect waves-dark" href="{{url('/admin/subCategory')}}" aria-expanded="false">
                            <i class="fa fa-home"></i>
                            <span class="hide-menu">  الاقسام الرئيسية</span>
                        </a>
                    </li>

                <li> 
                        <a class="waves-effect waves-dark" href="{{url('/admin/Subofsub')}}" aria-expanded="false">
                            <i class="fa fa-home"></i>
                            <span class="hide-menu">   الاقسام الفرعيه </span>
                        </a>
                    </li>

                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin/items')}}" aria-expanded="false">
                        <i class="fa fa-align-right"></i>
                        <span class="hide-menu">المقالات</span>
                    </a>
                </li>

                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin/product')}}" aria-expanded="false">
                        <i class="fa fa-inbox"></i>
                        <span class="hide-menu">المنتجات</span>
                    </a>
                </li>

                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin/users')}}" aria-expanded="false">
                        <i class="fa fa-users"></i>
                        <span class="hide-menu">المستخدمين</span>
                    </a>
                </li>
                
                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin/about_us')}}" aria-expanded="false">
                        <i class="fa fa-university"></i>
                        <span class="hide-menu">من نحن</span>
                    </a>
                </li>

                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin/contact_us')}}" aria-expanded="false">
                        <i class="fa fa-envelope"></i>
                        <span class="hide-menu">اتصل بنا</span>
                    </a>
                </li>

                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin/comment')}}" aria-expanded="false">
                        <i class="fa fa-comments"></i>
                        <span class="hide-menu">التعليقات</span>
                    </a>
                </li>

                <li> 
                    <a class="waves-effect waves-dark" href="{{url('/admin/orders')}}" aria-expanded="false">
                        <i class="fa fa-sticky-note"></i>
                        <span class="hide-menu">الطلبات المقدمه</span>
                    </a>
                </li>

                
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->