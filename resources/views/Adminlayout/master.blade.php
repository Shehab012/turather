<div class="col-md-12">
	<div class="row">
		@include('Adminlayout.header')
	</div>
</div>

		<div class="col-md-4">
			<div class="row">
				@include('Adminlayout.sidebar')
			</div>
		</div>
		
		<div class="col-md-12">
				@yield('content')
		</div>



<div class="col-md-12">
	<div class="row">
		@include('Adminlayout.footer')
	</div>
</div>