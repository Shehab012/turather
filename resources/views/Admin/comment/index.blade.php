@extends('Adminlayout.master')
@section('title', 'التعليقات')

@section('content')
<style>
    .pagination li {
        padding: 8px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 14px;
    }
</style>            
    
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                            @include('Adminlayout.errors')
                        <h4 class="card-title">التعليقات
                        </h4>
                        @if(count($data) > 0)
                        <table class="table">
                        	<tr>
                        		<th>الاسم</th>
                        		<th>البريد الالكتروني</th>
                        		<th>الرسال</th>
                        		<th>الحاله</th>
                        		<th>التفعيل</th>
                        		<th>حذف</th>
                        		<th>المقال</th>
                        	</tr>
                        <tbody>
                            @foreach($data as $comment)
                            <?php
                                $substr = subStr($comment->desc,-40);
                            ?>
                                <tr>
                                <td> <a href="/admin/commentshow/{{$comment->id}}"> {{ $comment->name }} </a> </td>
                                    <td> {{ $comment->email }} </td>
                                    <td> {{ $substr }} </td>
                                    @if($comment->type == 'unwatched')
                                <td> <a href="/admin/readcomment/{{ $comment->id }}" class="btn btn-success btn-sm"> قراءه </a> </td>
                                    @else
                                <td> <a href="/admin/unreadcommen/{{ $comment->id }}" class="btn btn-warning btn-sm">تمت قراءتها</a> </td>
                                    @endif
                                    @if($comment->status == 'desactive' )
                                <td> <a href="/admin/activecomment/{{ $comment->id }}" class="btn btn-info btn-sm"> تفعيل </a> </td>
                                    @else
                                        <td> <a href="/admin/disactivecomment/{{ $comment->id }}" class="btn btn-danger btn-sm"> التفعيل ايقاف </a> </td>
                                    @endif
                                <td><a onclick="return confirm('Are you sure?')" href="/admin/deletecomment/{{ $comment->id }}" class="btn btn-danger btn-sm"> <span class="fa fa-trash"></span> </a></td>
                                <?php
                                $item = \App\item::where('id',$comment->item_id)->first();
                                ?>
                                <td><a href="/admin/update_item/{{ $item->id }}">{{ $item->title_ar }}</a> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>    
                    {{ $data->links() }}    
                            @else
                            <div class="alert alert-success text-center">
                                <p><strong> لا توجد بيانات ف الوقت الحالي </strong></p>
                            </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection