@extends('Adminlayout.master')
@section('title', '  التعليقات ')

@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                            <li class="breadcrumb-item active"><a href="/admin/comment"> اعدادات التعليقات </a> </li>
                        </ol>
                    </div>
                </div>
            </div>
                <div class="card card-body bg-white">
                    <div class="form-group text-center"> <h3>{{ $data->name }}</h3> </div>
                    <div class="form-group text-center border"> <p><b> {{ $data->desc }} </b></p></div>
                    <div class="form-group pull-right border"> <small> {{ $data->created_at->format('Y-m-d') }} </small> </div>
                </div>
        </div>
    </div>
</div>
@endsection