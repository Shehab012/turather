@extends('Adminlayout.master')
@section('header')
<style>
    .pagination li{
        padding:5px;
    }
</style>

@endsection
@section('title')
الصفحة الرئيسية
@endsection
@section('content')
<?php
$contact = \App\contact::orderBy('id','desc')->limit(5)->get();
$users = \App\User::all();
$main_cat = \App\main_cat::all();
$item = \App\item::all();
$sub_cat = \App\sub_cat::all();
$comments = \App\comment::orderBy('id','Desc')->limit(5)->get();
?>
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Dashboard 1</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a target="_blank" href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard 1</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Info box -->
        <!-- ============================================================== -->
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h3><i class="fa fa-users"></i></h3>
                                    <p class="text-muted">المستخدمين</p>
                                </div>
                                <div class="ml-auto">
                                    <h2 class="counter text-primary">{{ count($users) }}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: {{ count($users) }}%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h3><i class="fa fa-sticky-note"></i></h3>
                                    <p class="text-muted">المقالات</p>
                                </div>
                                <div class="ml-auto">
                                    <h2 class="counter text-cyan">{{ count($item) }}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-cyan" role="progressbar" style="width: {{ count($item) }}%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h3><i class="fa fa-file"></i></h3>
                                    <p class="text-muted">الاقسام الرئسيه</p>
                                </div>
                                <div class="ml-auto">
                                    <h2 class="counter text-purple">{{ count($main_cat) }}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-purple" role="progressbar" style="width:{{ count($main_cat) }}% ; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h3><i class="fa fa-sitemap"></i></h3>
                                    <p class="text-muted">الاقسام الفرعيه</p>
                                </div>
                                <div class="ml-auto">
                                    <h2 class="counter text-success">{{ count($sub_cat) }}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: {{ count($sub_cat) }}%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
             <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h3><i class="fa fa-comment"></i></h3>
                                    <p class="text-muted">التعليقات</p>
                                </div>
                                <div class="ml-auto">
                                    <h2 class="counter text-purple">{{ count($comments) }}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-purple" role="progressbar" style="width:{{ count($comments) }}% ; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
             <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <div>
                                    <h3><i class="fa fa-envelope-open"></i></h3>
                                    <p class="text-muted">الرسائل</p>
                                </div>
                                <div class="ml-auto">
                                    <h2 class="counter text-purple">{{ count($contact) }}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-purple" role="progressbar" style="width:{{ count($contact) }}% ; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Info box -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Over Visitor, Our income , slaes different and  sales prediction -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-4 col-md-12">
                <div class="row">
                    <!-- Column -->
                    <!-- Column -->
                    <!-- Column -->
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Comment - table -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- ============================================================== -->
            <!-- Comment widgets -->
            <!-- ============================================================== -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body alert alert-danger">
                        <h5 class="card-title ">التعليقات</h5>
                    </div>
                    <!-- ============================================================== -->
                    <!-- Comment widgets -->
                    <!-- ============================================================== -->
                    <div class="comment-widgets">
                        <!-- Comment Row -->
                        @if(count($comments) == 0)
                        <div class="text-center">
                            <h3> لاتوجد تعليقات غير مفعله </h3>
                        </div>
                        @else
                        @foreach($comments as $comment)
                        <div class="d-flex no-block comment-row">
                            <div class="p-2"><span class="round"><img src="{{url('/back')}}/assets/images/users/1.jpg" alt="user" width="50"></span></div>
                            <div class="comment-text w-100">
                                <h5 class="font-medium"><a href="admin/commentshow/{{ $comment->id }}"> {{ $comment->name }}</a></h5>
                                <p class="m-b-10 text-muted">{{ $comment->desc }}</p>
                                <div class="comment-footer">
                                    <span class="text-muted pull-right">{{ $comment->created_at->format('Y-m-d') }}</span> <span class="badge badge-pill badge-info">Pending</span> <span class="action-icons">
                                        <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-check"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-heart"></i></a>    
                                    </span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        <!-- Comment Row -->
                        <!-- Comment Row -->
                        <!-- Comment Row -->
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- Table -->
            <!-- ============================================================== -->
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body alert alert-danger">
                        <h5 class="card-title ">رسائل الموقع</h5>
                    </div>
                    <!-- ============================================================== -->
                    <!-- Comment widgets -->
                    <!-- ============================================================== -->
                    <div class="comment-widgets">
                        <!-- Comment Row -->
                        @if(count($contact) == 0)
                        <div class="text-center">
                            <h3>لاتوجد رسائل للموقع حاليا</h3>
                        </div>
                        @else
                        @foreach($contact as $con)
                        <div class="d-flex no-block comment-row">
                            <div class="p-2"><span class="round"><img src="{{url('/back')}}/assets/images/users/1.jpg" alt="user" width="50"></span></div>
                            <div class="comment-text w-100">
                                <h5 class="font-medium">{{ $contact->fname }} {{$contact->lname }}</h5>
                                <p class="m-b-10 text-muted">{{ $contact->massege }}</p>
                                <div class="comment-footer">
                                    <span class="text-muted pull-right">{{$contact->created_at->format('Y-m-d')}}</span> <span class="badge badge-pill badge-info">Pending</span> <span class="action-icons">
                                        <a href="javascript:void(0)"><i class="ti-pencil-alt"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-check"></i></a>
                                        <a href="javascript:void(0)"><i class="ti-heart"></i></a>    
                                    </span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


        @endsection