@extends('Adminlayout.master')
@section('header')

@section('title', 'اعدادات الموقع الرئيسية')

<link href="{{url('/back')}}/dist/css/pages/tab-page.css" rel="stylesheet">
<link href="{{url('/back')}}/dist/css/pages/breadcrumb-page.css" rel="stylesheet">
<link href="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
        height: 400px;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: -113px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
    }

    #pac-input:focus {
        border-color: #4d90fe;
    }

    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }
    #target {
        width: 345px;
    }
</style>
@endsection

@section('content')
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @include('Adminlayout.errors')
                        <h4 class="card-title">اعدادات الموقع الرئيسية</h4>
                        <!-- Nav tabs -->
                        <div class="vtabs col-lg-12">
                            {{-- <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home4" role="tab">
                                        <span class="hidden-sm-up"><i class="ti-home"></i></span>
                                        <span class="hidden-xs-down">الرئيسية</span> 
                                    </a> 
                                </li>
                                <li class="nav-item"> 
                                    <a class="nav-link" data-toggle="tab" href="#profile4" role="tab">
                                        <span class="hidden-sm-up"><i class="fa fa-picture-o"></i></span>
                                        <span class="hidden-xs-down">اللوجو</span>
                                    </a> 
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#messages4" role="tab">
                                        <span class="hidden-sm-up"><i class="fa fa-search-plus"></i></span>
                                        <span class="hidden-xs-down">تهيئة الموقع  SEO</span>
                                    </a> 
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#social" role="tab">
                                        <span class="hidden-sm-up"><i class="fa fa-facebook"></i></span>
                                        <span class="hidden-xs-down">  السوشيل ميديا</span>
                                    </a> 
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#mappedit" role="tab">
                                        <span class="hidden-sm-up"><i class="fa fa-map-marker"></i></span>
                                        <span class="hidden-xs-down"> الخريطة</span>
                                    </a> 
                                </li>
                            </ul> --}}
                            <!-- Tab panes -->
                            <div class="tab-content ">
                                    @if(empty($setting))
                                        <form enctype="multipart/form-data" action="/admin/mainsetting" method="post">
                                            
                                            {{csrf_field()}}

                                            <div class="tab-pane active " id="home4" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header bg-info">
                                                            <h4 class="m-b-0 text-white"> الرئيسية</h4>
                                                        </div>
                                                        <div class="card-body">
                                                                    <div class="form-body">
                                                                        <div class="row p-t-20">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group   {{ $errors->has('website_name_ar') ? ' has-danger' : '' }}">
                                                                                    <label>اسم الموقع عربى</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_name_ar"  placeholder="اسم الموقع عربى"  >
                                                                                    </div>
                                                                                    @if ($errors->has('website_name_ar'))
                                                                                    <small class="form-control-feedback">{{ $errors->first('website_name_ar') }} </small>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group   {{ $errors->has('website_name_en') ? ' has-danger' : '' }}">
                                                                                    <label>اسم الموقع انجليزى</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_name_en" placeholder="اسم الموقع انجليزى" >
                                                                                    </div>
                                                                                    @if ($errors->has('website_name_en'))
                                                                                    <small class="form-control-feedback">{{ $errors->first('website_name_en') }} </small>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">العنوان عربى</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_address_ar"  placeholder="العنوان عربى">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">العنوان انجليزى</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_address_en"   placeholder="العنوان انجليزى">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">الايميل</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_email" placeholder="الايميل">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">الهاتف</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_phone" placeholder="الهاتف">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                
                                                                        <div class="col-md-6 form-group {{ $errors->has('website_about_ar') ? ' has-danger' : '' }}">
                                                                                <div class="form-group">
                                                                                    <label class="control-label">عن الموقع عربى</label>
                                                                                    <textarea maxlength="246" class="form-control" name="website_about_ar" rows="5"></textarea>
                                                                                    @if ($errors->has('website_about_ar'))
                                                                                        <small class="form-control-feedback">{{ $errors->first('website_about_ar') }} </small>
                                                                                    @endif                        
                                                                                </div>
                                                                            </div>
                                                                        <div class="col-md-6 form-group {{ $errors->has('website_about_en') ? ' has-danger' : '' }}">
                                                                                <div class="form-group">
                                                                                    <label class="control-label">عن الموقع انجليزى</label>
                                                                                    <textarea class="form-control" maxlength="246" name="website_about_en" rows="5"></textarea>
                                                                                    @if ($errors->has('website_about_en'))
                                                                                        <small class="form-control-feedback">{{ $errors->first('website_about_en') }} </small>
                                                                                    @endif                        
                                                                                </div>
                                                                            </div>
                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                {{-- =========================================================== --}}
                
                                                <div class="tab-pane" id="profile4" role="tabpanel">
                                                        <div class="card">
                                                            <div class="card-header bg-info">
                                                                <h4 class="m-b-0 text-white"> اللوجو</h4>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="form-body">
                                                                    <div class="row p-t-20">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">اللوجو عربى</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                    </div>
                                                                                    <input type="file" name="website_logo_ar" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">اللوجو انجليزى</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                    </div>
                                                                                    <input type="file" name="website_logo_en"  class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">اللوجو فوتر عربي</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                    </div>
                                                                                    <input type="file" name="website_logo_footer_ar"  class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">اللوجو فوتر انجليزي</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                    </div>
                                                                                    <input type="file" name="website_logo_footer_en"  class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">ايقونة الموقع</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                    </div>
                                                                                    <input type="file" name="website_icon"  class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            {{-- ================================================================ --}}

                                            <div class="tab-pane p-20" id="messages4" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header bg-info">
                                                            <h4 class="m-b-0 text-white"> SEO</h4>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-body">
                                                                <div class="row p-t-20">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputuname">الكلمات المفتاحية عربى</label>
                                                                            <div class="input-group mb-3">
                                                                                <input type="text" name="website_keywor_ar"  data-role="tagsinput" placeholder="add tags" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputuname">الكلمات المفتاحية انجليزى</label>
                                                                            <div class="input-group mb-3">
                                                                                <input type="text" name="website_keywor_en"  data-role="tagsinput" placeholder="add tags" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">وصف الموقع عربى</label>
                                                                            <textarea class="form-control" name="website_desc_ar" rows="5"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">وصف الموقع انجليزى</label>
                                                                            <textarea class="form-control" name="website_desc_en" rows="5"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                        {{-- ================================================= --}}

                                        <div class="tab-pane p-20" id="social" role="tabpanel">
                                                <div class="card">
                                                    <div class="card-header bg-info">
                                                        <h4 class="m-b-0 text-white"> سوشيل ميديا</h4>
                                                    </div>  
                                                            <div class="card-body">
                                                                <div class="form-body">
                                                                    <div class="row p-t-20">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Facebook</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-facebook"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_facebook"  class="form-control" placeholder="Facebook" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Twitter</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-twitter"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_twitter"  class="form-control" placeholder="Twitter" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Google-Plus</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-google-plus"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_google" class="form-control" placeholder="Google-Plus" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Youtube</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-youtube"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_youtube"  class="form-control" placeholder="Youtube" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
            
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Instagram</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-instagram"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_instgram"  class="form-control" placeholder="Instagram" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">LinkedIn</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-linkedin"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_linked_in"  class="form-control" placeholder="LinkedIn" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                {{-- ======================= --}}
            
                                            
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> حفظ</button>
                                            </div>
                                        </form>

                                        @else

                                        <form enctype="multipart/form-data" action="/admin/editmainsetting/{{$setting->id}}" method="post">                                            
                                            {{ csrf_field()}}
                                            <div class="tab-pane active " id="home4" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header bg-info">
                                                            <h4 class="m-b-0 text-white"> الرئيسية</h4>
                                                        </div>
                                                        <div class="card-body">
                                                                    <div class="form-body">
                                                                        <div class="row p-t-20">
                                                                            <div class="col-md-6">
                                                                                <div class="form-group   {{ $errors->has('website_name_ar') ? ' has-danger' : '' }}">
                                                                                    <label>اسم الموقع عربى</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_name_ar" @if(!empty($setting->website_name_ar)) value="{{$setting->website_name_ar}}" @endif placeholder="اسم الموقع عربى"  >
                                                                                    </div>
                                                                                    @if ($errors->has('website_name_ar'))
                                                                                    <small class="form-control-feedback">{{ $errors->first('website_name_ar') }} </small>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group   {{ $errors->has('website_name_en') ? ' has-danger' : '' }}">
                                                                                    <label>اسم الموقع انجليزى</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_name_en" @if(!empty($setting->website_name_en)) value="{{$setting->website_name_en}}" @endif placeholder="اسم الموقع انجليزى" >
                                                                                    </div>
                                                                                    @if ($errors->has('website_name_en'))
                                                                                    <small class="form-control-feedback">{{ $errors->first('website_name_en') }} </small>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">العنوان عربى</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_address_ar" @if(!empty($setting->website_address_ar)) value="{{$setting->website_address_ar}}" @endif placeholder="العنوان عربى">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">العنوان انجليزى</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_address_en" @if(!empty($setting->website_address_en)) value="{{$setting->website_address_en}}" @endif  placeholder="العنوان انجليزى">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">الايميل</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_email" @if(!empty($setting->website_email)) value="{{$setting->website_email}}" @endif placeholder="الايميل">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">الهاتف</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control" name="website_phone" @if(!empty($setting->website_phone)) value="{{$setting->website_phone}}" @endif placeholder="الهاتف">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                
                                                                        <div class="col-md-6 form-group {{ $errors->has('website_about_ar') ? ' has-danger' : '' }}">
                                                                                <div class="form-group">
                                                                                    <label class="control-label">عن الموقع عربى</label>
                                                                                    <textarea maxlength="246" class="form-control" name="website_about_ar" rows="5">@if(!empty($setting->website_about_ar)){{$setting->website_about_ar}} @endif</textarea>
                                                                                    @if ($errors->has('website_about_ar'))
                                                                                        <small class="form-control-feedback">{{ $errors->first('website_about_ar') }} </small>
                                                                                    @endif                        
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6 form-group {{ $errors->has('website_about_en') ? ' has-danger' : '' }}">
                                                                                <div class="form-group">
                                                                                    <label class="control-label">عن الموقع انجليزى</label>
                                                                                    <textarea maxlength="246" class="form-control" name="website_about_en" rows="5">@if(!empty($setting->website_about_en)) {{$setting->website_about_en}} @endif</textarea>
                                                                                    @if ($errors->has('website_about_en'))
                                                                                        <small class="form-control-feedback">{{ $errors->first('website_about_en') }} </small>
                                                                                    @endif                        
                                                                                </div>
                                                                            </div>
                
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                {{-- =========================================================== --}}
                
                                                <div class="tab-pane" id="profile4" role="tabpanel">
                                                        <div class="card">
                                                            <div class="card-header bg-info">
                                                                <h4 class="m-b-0 text-white"> اللوجو</h4>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="form-body">
                                                                    <div class="row p-t-20">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">اللوجو عربى</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                    </div>
                                                                                    <input type="file" name="website_logo_ar" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                            @if(!empty($setting->website_logo_ar))
                                                                            <a href="{{asset('webImage/'.$setting->website_logo_ar)}}" data-toggle="lightbox">
                                                                            <img src="{{asset('webImage/'.$setting->website_logo_ar)}}" style="height: 96px;"class="img-fluid">
                                                                            </a>
                                                                            @endif
                                                                            <br> <br> <br>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">اللوجو انجليزى</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                    </div>
                                                                                    <input type="file" name="website_logo_en"  class="form-control">
                                                                                </div>
                                                                            </div>
                                                                            @if(!empty($setting->website_logo_en))
                                                                            <a href="{{asset('webImage/'.$setting->website_logo_en)}}" data-toggle="lightbox">
                                                                                <img src="{{asset('webImage/'.$setting->website_logo_en)}}" style="height: 96px;"class="img-fluid">
                                                                            </a>
                                                                            @endif
                                                                            <br> <br> <br>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">اللوجو فوتر عربي</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                        </div>
                                                                                        <input type="file" name="website_logo_footer_ar"  class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                @if(!empty($setting->website_logo_footer_ar))
                                                                                <a href="{{asset('webImage/'.$setting->website_logo_footer_ar)}}" data-toggle="lightbox">
                                                                                    <img src="{{asset('webImage/'.$setting->website_logo_footer_ar)}}" style="height: 96px;"class="img-fluid">
                                                                                </a>
                                                                                @endif
                                                                                <br> <br> <br>
    
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <label for="exampleInputuname">اللوجو فوتر انجليزي</label>
                                                                                    <div class="input-group mb-3">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                        </div>
                                                                                        <input type="file" name="website_logo_footer_en"  class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                @if(!empty($setting->website_logo_footer_en))
                                                                                <a href="{{asset('webImage/'.$setting->website_logo_footer_en)}}" data-toggle="lightbox">
                                                                                    <img src="{{asset('webImage/'.$setting->website_logo_footer_en)}}" style="height: 96px;"class="img-fluid">
                                                                                </a>
                                                                                @endif
                                                                                <br> <br> <br>
                                                                            </div>
    
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">ايقونة الموقع</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text"><i class="fa fa-picture-o"></i></span>
                                                                                    </div>
                                                                                    <input type="file" name="website_icon"  class="form-control" >
                                                                                </div>
                                                                            </div>
                                                                            @if(!empty($setting->website_icon))
                                                                            <a href="{{asset('webImage/'.$setting->website_icon)}}" data-toggle="lightbox">
                                                                                <img src="{{asset('webImage/'.$setting->website_icon)}}" style="height: 96px;"class="img-fluid">
                                                                            </a>
                                                                            @endif
                                                                            <br> <br> <br>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            {{-- ================================================================ --}}

                                            <div class="tab-pane p-20" id="messages4" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-header bg-info">
                                                            <h4 class="m-b-0 text-white"> SEO</h4>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="form-body">
                                                                <div class="row p-t-20">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputuname">الكلمات المفتاحية عربى</label>
                                                                            <div class="input-group mb-3">
                                                                                <input type="text" name="website_keywor_ar" @if(!empty($setting->website_keywor_ar)) value="{{$setting->website_keywor_ar}}" @endif data-role="tagsinput" placeholder="add tags" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputuname">الكلمات المفتاحية انجليزى</label>
                                                                            <div class="input-group mb-3">
                                                                                <input type="text" name="website_keywor_en" @if(!empty($setting->website_keywor_en)) value="{{$setting->website_keywor_en}}" @endif data-role="tagsinput" placeholder="add tags" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">وصف الموقع عربى</label>
                                                                            <textarea class="form-control" name="website_desc_ar" rows="5">@if(!empty($setting->website_desc_ar)) {{$setting->website_desc_ar}} @endif</textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="control-label">وصف الموقع انجليزى</label>
                                                                            <textarea class="form-control" name="website_desc_en" rows="5">@if(!empty($setting->website_desc_en)) {{$setting->website_desc_en}} @endif</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                        {{-- ================================================= --}}

                                        <div class="tab-pane p-20" id="social" role="tabpanel">
                                                <div class="card">
                                                    <div class="card-header bg-info">
                                                        <h4 class="m-b-0 text-white"> سوشيل ميديا</h4>
                                                    </div>  
                                                            <div class="card-body">
                                                                <div class="form-body">
                                                                    <div class="row p-t-20">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Facebook</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-facebook"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_facebook" @if(!empty($setting->website_facebook)) value="{{$setting->website_facebook}}" @endif class="form-control" placeholder="Facebook" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Twitter</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-twitter"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_twitter" @if(!empty($setting->website_twitter)) value="{{$setting->website_twitter}}" @endif class="form-control" placeholder="Twitter" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Google-Plus</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-google-plus"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_google" @if(!empty($setting->website_google)) value="{{$setting->website_google}}" @endif class="form-control" placeholder="Google-Plus" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Youtube</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-youtube"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_youtube" @if(!empty($setting->website_youtube)) value="{{$setting->website_youtube}}" @endif class="form-control" placeholder="Youtube" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
            
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">Instagram</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-instagram"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_instgram" @if(!empty($setting->website_instgram)) value="{{$setting->website_instgram}}" @endif class="form-control" placeholder="Instagram" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label for="exampleInputuname">LinkedIn</label>
                                                                                <div class="input-group mb-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <span class="input-group-text" ><i class="fa fa-linkedin"></i></span>
                                                                                    </div>
                                                                                    <input type="text" name="website_linked_in" @if(!empty($setting->website_linked_in)) value="{{$setting->website_linked_in}}" @endif class="form-control" placeholder="LinkedIn" >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                {{-- ======================= --}}
            
                                            
                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i> حفظ</button>
                                            </div>

                                        </form>
                                        @endif
        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<script src="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtaIwmepScDMmCOqr7-WszNY0HU4uwhdY&libraries=places&callback=initAutocomplete"
async defer></script>


<!--  -->
@endsection
@endsection