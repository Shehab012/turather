@extends('Adminlayout.master')
@section('title', 'الطلبات')

@section('content')
<style>
    .pagination li {
        padding: 8px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 14px;
    }
</style>            
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"> الطلبات </h4>
                            @include('Adminlayout.errors')
                            <div class="card-header bg-danger form-group">
                                <h4 class="m-b-0 text-white">الطلبات المقدمه</h4>
                            </div>            
                            @if(count($orders) > 0)
                            <table class="table table-striped">
                                    <thead>
                                            <tr>
                                                <th>رقم الطلب</th>
                                                <th>اسم المرسل</th>
                                                <th>البريد الالكتروني للمرسل</th>
                                                <th>اسم المنتج</th>
                                                <th> المشاهدة </th>
                                                <th></th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                        @foreach( $orders as $data )
                                            <tr>
                                                <td> {{ $data->id }} </td>
                                                <td><a href="/admin/order/{{ $data->id }}"> {{ $data->first_name }} {{ $data->last_name }} </a> </td>
                                                <td> {{ $data->email }}</td>
                                                <?php
                                                    $product = \App\product::where('id',$data->product_id)->first();
                                                ?>
                                                <td> <a target="_blank" href="/admin/updateproduct/{{ $product->id }}"> {{ $product->product_title_ar }} </a></td>
                                                @if($data->status == 1)
                                                    <td> <span class="btn btn-sm btn-danger"> غير مشاهد </span> </td>
                                                @elseif($data->status == 2)
                                                    <td> <span class="btn btn-sm btn-info">  مشاهد </span> </td>                                                
                                                @endif
                                                <td> <a onclick="return confirm('Are you sure?')" href="/admin/deleteorder/{{ $data->id }}" class="btn btn-danger btn-sm"> حذف الطلب</a></td>
                                            </tr>
                                        @endforeach
                            {{ $orders->links() }}
                            @else
                            <div class="alert alert-success text-center">
                                <p><strong> لا توجد بيانات ف الوقت الحالي </strong></p>
                            </div>
                            @endif

                                    </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection