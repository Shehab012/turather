@extends('Adminlayout.master')
@section('title', 'الطلب')

@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"> الطلبات </h4>
                        @include('Adminlayout.errors')
                        <div class="card-header bg-danger form-group">
                            <h4 class="m-b-0 text-white"> الطلب المقدم </h4>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="">الاسم بالكامل </label>
                                <input type="text" readonly class="form-control" value="{{ $data->first_name }} {{ $data->last_name }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for=""> البريد الالكتروني </label>
                                <input type="text" readonly class="form-control" value="{{ $data->email }}" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for=""> رقم الهاتف </label>
                                <input type="text" readonly class="form-control" value="{{ $data->phone }}" >
                            </div>
                            <div class="col-md-12 form-group">
                                <label for=""> الطلب </label>
                                <textarea name="" class="form-control" readonly rows="5">{{ $data->note }}</textarea>
                            </div>
                        <hr>
                            <div class="text-center col-md-12"> 
                                <label for=""> تم الاستقبال في : </label>                                    
                                <b> {{ $data->created_at->format('Y-m-d') }}  </b>    
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>   
@endsection
