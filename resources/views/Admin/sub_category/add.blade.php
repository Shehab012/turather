@extends('Adminlayout.master')
@section('title', 'اضافه قائمه فرعيه')

@section('header')
<link href="{{url('/back')}}/dist/css/pages/tab-page.css" rel="stylesheet">
<link href="{{url('/back')}}/dist/css/pages/breadcrumb-page.css" rel="stylesheet">
<link href="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                            <li class="breadcrumb-item active"><a href="/admin/subCategory">الاعدادات الفرعيه</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @include('Adminlayout.errors')
                        <form class="form-group" method="POST" action="/admin/add_sub_category">
                            <div class="form-body">
                            {{ csrf_field() }}
                            <div class="card-header bg-danger form-group">
                                <h4 class="m-b-0 text-white">اضافة فرع جديد</h4>
                            </div>
                            <div class="col-md-12 form-group {{ $errors->has('main_cat_id') ? ' has-danger' : '' }}">
                                    <div class="row">
                                        <label> <b> القائمه الرئسيه </b>  </label>
                                        <select class="form-control" name="main_cat_id">
                                            @foreach($data as $main)
                                        <option value="{{ $main->id }}"> {{ $main->cat_name_ar }} </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('main_cat_id'))
                                        <small class="form-control-feedback">{{ $errors->first('main_cat_id') }} </small>
                                        @endif                    
                                    </div>
                                </div>
    
                            <div class="form-group col-12">
                                <div class="row">                                    
                                    <div class="col-md-6 form-group {{ $errors->has('name_ar') ? ' has-danger' : '' }}">
                                        <label> <b>الاسم بالعربيه</b></label>
                                            <input type="text" name="name_ar" class="form-control"> 
                                            @if ($errors->has('name_ar'))
                                            <small class="form-control-feedback">{{ $errors->first('name_ar') }} </small>
                                            @endif                    

                                        </div>
                                        <div class="col-md-6 form-group {{ $errors->has('name_en') ? ' has-danger' : '' }}">
                                            <label> <b>الاسم بالانجليزيه</b></label>
                                            <input type="text" name="name_en" class="form-control">
                                            @if ($errors->has('name_en'))
                                            <small class="form-control-feedback">{{ $errors->first('name_en') }} </small>
                                            @endif                    

                                        </div>
                                </div>
                            </div>
                            <div class="form-group col-12">
                                    <div class="row">
                                        <div class="col-md-6 form-group {{ $errors->has('order') ? ' has-danger' : '' }}">
                                            <label> <b>الترتيب</b></label>
                                            <input type="number" name="order" class="form-control">
                                            @if ($errors->has('order'))
                                            <small class="form-control-feedback">{{ $errors->first('order') }} </small>
                                            @endif                    
                                        </div>
                                        <div class="col-md-6 form-group {{ $errors->has('status') ? ' has-danger' : '' }}">
                                            <label><b> التفعيل </b> </label>
                                            <select name="status" class="form-control">
                                                <option value="1" selected>تفعيل</option>
                                                <option value="2"> توقيف </option>
                                            </select>
                                            @if ($errors->has('status'))
                                            <small class="form-control-feedback">{{ $errors->first('status') }} </small>
                                            @endif                    

                                        </div>
                                    </div>
                            </div>

                            <div class="card-header bg-danger form-group">
                                <h4 class="m-b-0 text-white">تهيئة القسم (SEO)</h4>
                            </div>    

                            <div class="form-group col-12">
                                    <div class="row">                                    
                                        <div class="col-md-6 form-group {{ $errors->has('desc_ar') ? ' has-danger' : '' }}">
                                            <label> <b>الوصف بالغه العربيه</b></label>
                                                <textarea rows=4 name="desc_ar" class="form-control"> </textarea> 
                                                @if ($errors->has('desc_ar'))
                                                <small class="form-control-feedback">{{ $errors->first('desc_ar') }} </small>
                                                @endif                        
                                            </div>
                                            <div class="col-md-6 form-group {{ $errors->has('desc_en') ? ' has-danger' : '' }}">
                                                <label> <b>الوصف بالغه الانجليزيه</b></label>
                                                <textarea rows=4 name="desc_en" class="form-control"></textarea>
                                                @if ($errors->has('desc_en'))
                                                <small class="form-control-feedback">{{ $errors->first('desc_en') }} </small>
                                                @endif                        

                                            </div>
                                    </div>
                                </div>

                                <div class="form-group col-12">
                                        <div class="row">                                    
                                            <div class="col-md-6 form-group {{ $errors->has('seo_ar') ? ' has-danger' : '' }}">
                                                <label> <b>الكلمات المفتاحيه بالعربيه</b></label>
                                                    <input type="text" data-role="tagsinput" name="seo_ar" class="form-control"> 
                                                    @if ($errors->has('seo_ar'))
                                                    <small class="form-control-feedback">{{ $errors->first('seo_ar') }} </small>
                                                    @endif                    
                
                                                </div>
                                                <div class="col-md-6 form-group {{ $errors->has('seo_en') ? ' has-danger' : '' }}">
                                                    <label> <b>الكلمات المفتاحيه بالانجليزيه</b></label>
                                                    <input type="text" data-role="tagsinput" name="seo_en" class="form-control">
                                                    @if ($errors->has('seo_en'))
                                                    <small class="form-control-feedback">{{ $errors->first('seo_en') }} </small>
                                                    @endif     
                                                </div>
                                        </div>
                                    </div>        
    
                                <div class="col-md-12 form-group">
                                    <div class="text-left">       
                                        <div class="pull-left">
                                            <button class="btn btn-success" ><span class="fa fa-send"> </span> اضافه</button>
                                        </div>                   
                                        <div class="pull-right">
                                            <a href="/admin/subCategory" class="btn btn-danger"> <span class="fa fa-sign-out"> </span> العوده </a>                                                
                                        </div>              
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<script src="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
@endsection

@endsection