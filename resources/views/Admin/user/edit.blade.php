@extends('Adminlayout.master')
@section('title', 'تعديل مستخدم')
@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                            <li class="breadcrumb-item active"><a href="/admin/users">اعدادات المستخدم </a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12">
                        @include('Adminlayout.errors')
                        <form class="form-group" action="/admin/updateuser/{{ $data->id }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="card-header bg-danger form-group">
                                        <h4 class="m-b-0 text-white"> بيانات المستخدم </h4>
                                    </div>
                                    <div class="col-md-12 form-group {{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label for="Name"> اسم المستخدم </label>
                                    <input type="text" placeholder="اسم المستخدم" class="form-control" value="{{ $data->name }}" name="name" />
                                    @if ($errors->has('name'))
                                    <small class="form-control-feedback">{{ $errors->first('name') }} </small>
                                    @endif
                                    </div>
                                    <div class="col-md-12 form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <label for="email"> البريد الالكتروني </label>
                                    <input name="email" type="email" class="form-control" value="{{ $data->email }}" placeholder="Exampil@mail.com" />
                                    @if ($errors->has('email'))
                                    <small class="form-control-feedback">{{ $errors->first('email') }} </small>
                                    @endif
                                    </div>

                                    <div class="col-md-12 form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <label for="password"> كلمة المرور </label>
                                            <input type="password" name="password" class="form-control" />
                                            @if ($errors->has('password'))
                                            <small class="form-control-feedback">{{ $errors->first('password') }} </small>
                                        @endif
    
                                        </div>
    
                                        <div class="col-md-12 form-group {{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                                            <label for="password">تأكيد كلمة المرور </label>
                                            <input type="password" name="password_confirmation" class="form-control" />
                                            @if ($errors->has('password_confirmation'))
                                            <small class="form-control-feedback">{{ $errors->first('password_confirmation') }} </small>
                                        @endif
    
                                        </div>
    
    
                                    <div class="col-md-12 form-group">
                                            <div class="text-left">        
                                                <div class="pull-left">
                                                    <button class="btn btn-success" ><span class="fa fa-send"> </span> اضافه</button>
                                                </div>    
                                                <div class="pull-right">
                                                    <a href="/admin/users" class="btn btn-danger"> <span class="fa fa-sign-out"></span> رجوع </a>                                                                                            
                                                </div>                            
                                            </div>
                                        </div>
                                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection