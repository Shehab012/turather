@extends('Adminlayout.master')
@section('title', 'المستخدمين')
@section('content')
<style>
    .pagination li {
        padding: 8px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 14px;
    }
</style>                
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @include('Adminlayout.errors')
                            <h4 class="card-title"> المستخدمين </h4>
                            <div class="form-group">
                                <a href="{{url('/admin/adduser')}}" class="btn btn-success"> <span class="fa fa-plus"></span> اضافة مستخدم </a>                               
                            </div> 
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>اسم المستخم</th>
                                            <th> البريد الالكتروني </th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $user)
                                            <tr>
                                                <td> {{ $user->id }} </td>
                                                <td> {{ $user->name }} </td>
                                                <td> {{ $user->email }} </td>
                                            <td> <a href="/admin/edituser/{{ $user->id }}" class="btn btn-success btn-sm"> تعديل </a>

                                                @if($user->id != 1)
                                                    <a onclick="return confirm('Are you sure?')" href="/admin/deleteuser/{{ $user->id }}" class="btn btn-danger btn-sm"> حذف </a> </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $data->links() }}                                
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection