@extends('Adminlayout.master')
@section('title', 'اضافه مقال')

@section('header')
<link href="{{url('/back')}}/dist/css/pages/tab-page.css" rel="stylesheet">
<link href="{{url('/back')}}/dist/css/pages/breadcrumb-page.css" rel="stylesheet">
<link href="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />


@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                            <li class="breadcrumb-item active"><a href="/admin/items">اعدادات المقالات</a></li>                            
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @include('Adminlayout.errors')
                            <form class="form-group" method="POST" enctype="multipart/form-data"  action="/admin/additem">
                                {{ csrf_field() }}
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="">
                                    <div class="card-header bg-danger form-group">
                                        <h4 class="m-b-0 text-white">اضافه اسم المقال</h4>
                                    </div>            
                                <div class="form-group col-md-12">
                                    <div class="row">
                                                <div class="col-md-6 form-group {{ $errors->has('title_ar') ? ' has-danger' : '' }}">
                                                    <label> <b> اسم المنتج عربي </b> </label>
                                                    <input class="form-control" name="title_ar" />
                                                    @if ($errors->has('title_ar'))
                                                        <small class="form-control-feedback">{{ $errors->first('title_ar') }} </small>
                                                    @endif                    
                                                </div>
                                                <div class="col-md-6 form-group {{ $errors->has('title_en') ? ' has-danger' : '' }}">
                                                        <label> <b> اسم المنتج انجليزي </b> </label>
                                            <input class="form-control" name="title_en" />
                                            @if ($errors->has('title_en'))
                                            <small class="form-control-feedback">{{ $errors->first('title_en') }} </small>
                                            @endif                    

                                        </div>  
                                    </div>
                                </div>

                                <div class="card-header bg-danger form-group">
                                        <h4 class="m-b-0 text-white">عن المقال </h4>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <div class="row">
                                                <div class="col-md-12 form-group {{ $errors->has('sub_cat_id') ? ' has-danger' : '' }}">
                                                    <label> <b> القائمه الفرعيه </b> </label>
                                                    <select id="main_id"  name="sub_cat_id" class="form-control" >
                                                        @foreach($sub as $s)
                                                            <option value="{{ $s->id }}"> {{ $s->sub_cat_name_ar }} </option>
                                                        @endforeach                                                            
                                                    </select>
                                                    @if ($errors->has('sub_cat_id'))
                                                    <small class="form-control-feedback">{{ $errors->first('sub_cat_id') }} </small>
                                                    @endif                        
                                                </div>

                                                    <div class="col-md-12 form-group">
                                                       
                                                        <label for=""> اختار القسم </label>
                                                        
                                                        <select name="sub_id" class="form-control">
                                                            <option value="">اختر</option>
                                                            <option></option>                                                                  
                                                        </select>
                                                   </div>

                                                    

        
                                            <div class="col-md-6 form-group {{ $errors->has('feu') ? ' has-danger' : '' }}">
                                                <label> <b> التصنيف </b> </label>
                                                <select class="form-control" name="feu">
                                                    <option value="1"> عاديه </option>
                                                    <option value="2"> مميزه </option>
                                                    @if ($errors->has('feu'))
                                                    <small class="form-control-feedback">{{ $errors->first('feu') }} </small>
                                                    @endif                        
                                                </select>
                                            </div>
                                            <div class="col-md-6 form-group {{ $errors->has('status') ? ' has-danger' : '' }}">
                                                <label> <b> الحاله </b> </label>
                                                <select class="form-control" name="status">
                                                    <option value="1"> تقعيل </option>
                                                    <option value="2"> توقيف </option>
                                                    @if ($errors->has('status'))
                                                    <small class="form-control-feedback">{{ $errors->first('status') }} </small>
                                                    @endif                        
                                                </select>
                                            </div>
                                        </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12 form-group {{ $errors->has('Image') ? ' has-danger' : '' }}">
                                                <label> <b> الصوره </b> </label>
                                                <input type="file" name="Image" class="form-control">
                                                @if ($errors->has('Image'))
                                                <small class="form-control-feedback">{{ $errors->first('Image') }} </small>
                                                @endif                        
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-header bg-danger form-group">
                                    <h4 class="m-b-0 text-white">وصف  مصغر المقال</h4>
                                </div>
                                
                                <div class="col-md-12 form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label><b> الوصف المصغر عربي</b> </label>
                                            <input maxlength="111" type="text" name="desc_sm_ar" class="form-control"/>
                                        </div>
                                        <div class="col-md-6">
                                                <label><b> الوصف المصغر انجليزي</b> </label>
                                                <input maxlength="111" type="text" name="desc_sm_en" class="form-control"/>
                                        </div>
                                    </div>
                                </div>



                                    <div class="card-header bg-danger form-group">
                                        <h4 class="m-b-0 text-white">وصف  الكامل المقال</h4>
                                    </div>


                                    <div class="col-md-12 form-group">
                                        <div class="row">
                                            <div class="col-md-12 form-group {{ $errors->has('desc_ar') ? ' has-danger' : '' }}">
                                                <label> <b> الوصف بالعربي </b> </label>
                                                <textarea id="tarea"rows=5 class="form-control" name="desc_ar"> </textarea>
                                                @if ($errors->has('desc_ar'))
                                                <small class="form-control-feedback">{{ $errors->first('desc_ar') }} </small>
                                                @endif                    
                                            </div>
                                            <div class="col-md-12 form-group {{ $errors->has('desc_en') ? ' has-danger' : '' }}">
                                                <label> <b> الوصف بالانجليزي </b> </label>
                                                <textarea id="tarea2" rows=5 class="form-control" name="desc_en"> </textarea>    
                                                @if ($errors->has('desc_en'))
                                                <small class="form-control-feedback">{{ $errors->first('desc_en') }} </small>
                                                @endif                    
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6 form-group {{ $errors->has('seo_ar') ? ' has-danger' : '' }}">
                                                    <label> <b> الكلمات المفتاحيه بالعربي </b> </label>
                                                    <input class="form-control" data-role="tagsinput" name="seo_ar" />
                                                    @if ($errors->has('seo_ar'))
                                                    <small class="form-control-feedback">{{ $errors->first('seo_ar') }} </small>
                                                    @endif                    
        
                                                </div>
    
                                                <div class="col-md-6 form-group {{ $errors->has('seo_en') ? ' has-danger' : '' }}">
                                                    <label> <b> الكلمات المفتاحيه بالانجليزي </b> </label>
                                                    <input class="form-control" data-role="tagsinput" name="seo_en" />
                                                    @if ($errors->has('seo_en'))
                                                    <small class="form-control-feedback">{{ $errors->first('seo_en') }} </small>
                                                    @endif                    
    
                                                </div>
                                            </div>
                                        </div>
    
                                    
                                    <div class="col-md-12 form-group">
                                        <div class="text-left">
                                            <div class="pull-left">
                                                <button class="btn btn-success" ><span class="fa fa-send"> </span> اضافه</button>                                                    
                                            </div>             
                                            <div class="pull-right">
                                                <a href="/admin/items" class="btn btn-danger"> <span class="fa fa-sign-out"> </span> العوده </a>
                                            </div>                         
                                        </div>
                                    </div>
                                    <?php $users = \App\User::all()->count(); ?>
                                </form>
                        </div>
                    </div>
            </div>    
        </div>
    </div>
</div>
@section('js')
<script>
$( "#main_id" ).change(function() {
    var place = $('#place');
    var token = $("input[name='_token']").val();
    var sub_cat_id = $(this).val();
    
    $.ajax({
			method: "POST",
            url: "<?php echo route('ajax-open') ?>",
            data: {sub_cat_id: sub_cat_id, _token: token},

		})
		.done(function (data) {
                $("select[name='sub_id'").html('');
                $("select[name='sub_id'").html(data.options);
        });
        
    });
</script>

{{-- <script type="text/javascript">
    $("select[name='sub_cat_id']").change(function () {
        var sub_cat_id = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: "<?php echo route('ajax-open') ?>",
            method: 'POST',
            data: {sub_cat_id: sub_cat_id, _token: token},
            success: function (data) {
                $("select[name='sub_id'").html('');
                $("select[name='sub_id'").html(data.options);
            }
        });
    });
</script> --}}



<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'tarea' );
</script>
<script>
    CKEDITOR.replace( 'tarea2' );
</script>

<script src="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
@endsection
@endsection
