@extends('Adminlayout.master')
@section('title', 'المقالات')

@section('content')
<style>
    .pagination li {
        padding: 8px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 14px;
    }
</style>            
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="#">اعدادات الموقع</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"> المنتجات </h4>
                            @include('Adminlayout.errors')
                            <div class="pull-left form-group">
                                    <a href="/admin/additem" class="btn btn-success"> <span class="fa fa-plus"></span> اضافة مقال </a>
                            </div>
                            <table class="table table-striped text-center">
                                    <thead>
                                            <tr>
                                                <th>الرقم</th>
                                                <th>الاسم بالعربي</th>
                                                <th>الاسم بالانجليزي</th>
                                                <th>مفعل</th>
                                                <th>عدد مرات المشاهده</th>
                                                <th></th>
                                            </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($data as $main)
                                            <tr>
                                                <td> {{ $main->id }} </td>
                                                <td> <a href="/admin/main_cat_show/{{ $main->id }}"> {{ $main->title_ar }} </a></td>
            
                                                <td> {{ $main->title_en }} </td>
                                                @if(  $main->status  == 1 )
                                                    <td>
                                                        <span class="btn btn-info btn-sm"> نعم</span>
                                                    </td>
                                                @else
                                                    <td>
                                                        <span class="btn btn-danger btn-sm"> لا  </span>
                                                    </td>
                                                @endif
                                                <td> <span class="btn waves-effect waves-light btn-rounded btn-info btn-sm">{{ $main->viewers }}</span></td>
                                                <td></td>
            
                                                <td>
                                                <a href="/admin/update_item/{{ $main->id }}" class=" btn btn-info btn-sm">  <span class="fa fa-edit"></span>  تعديل</a>
                                                
                                                <a onclick="return confirm('Are you sure?')" href="/admin/delete_item/{{ $main->id }}" class=" btn btn-danger btn-sm"> <span class="fa fa-trash"></span>  حذف</a>
                                            </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                </table>
                                {{ $data->links() }}                                 
                        </div>
                    </div>
            </div>    
        </div>
    </div>
</div>
@endsection