@extends('Adminlayout.master')
@section('title', 'القوام الفرعيه')
@section('content')
<style>
    .pagination li {
        padding: 8px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 14px;
    }
</style>        
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">الاقسام الفرعيه</h4>
                        @include('Adminlayout.errors')
                        <div class="pull-left form-group">
		                        <a href="/admin/add_subofsub" class="btn btn-success"> <span class="fa fa-plus"></span> اضافة فرع جديد </a>
                        </div>
                            <table class="table table-striped">
                            <thead>
                                	<tr>
                                		<th>الرقم</th>
                                		<th>الاسم بالعربي</th>
                                		<th>الاسم بالانجليزي</th>
                                		<th>مفعل</th>
                                		<th>القائمة الرئسيه</th>
                                		<th></th>
                                	</tr>
                            </thead>
                            <tbody>
                                    @foreach($data as $main)
                                    <tr>
                                        <td> {{ $main->id }} </td>
                                        <td> <a href="/admin/main_cat_show/{{ $main->id }}"> {{ $main->sub_of_sub_name_ar }} </a></td>
    
                                        <td> {{ $main->sub_of_sub_name_en }} </td>
                                        @if(  $main->status  == 1 )
                                            <td>
                                                <span class="btn btn-info btn-sm"> نعم</span>
                                            </td>
                                        @else
                                            <td>
                                                <span class="btn btn-danger btn-sm"> لا  </span>
                                            </td>
                                        @endif
                                        <?php
                                            $sub_cat = \App\sub_cat::where('id',$main->sub_cat_id)->first();
                                        ?>
                                        <td> {{ $sub_cat->sub_cat_name_ar }} </td>    
                                        <td>
                                        <a href="/admin/update_sub_of_sub/{{ $main->id }}" class=" btn btn-info btn-sm">  <span class="fa fa-edit"></span>  تعديل</a>

                                        <a onclick="return confirm('Are you sure?')" href="/admin/deleteSubOfSub/{{ $main->id }}" class=" btn btn-danger btn-sm"> <span class="fa fa-trash"></span>  حذف</a>
                                    </td>
                                    </tr>
                                @endforeach
                                </tbody>
                        </table>
                        {{ $data->links() }}                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection