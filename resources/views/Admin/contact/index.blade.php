@extends('Adminlayout.master')
@section('title', 'اتصل بنا')

@section('content')
<style>
    .pagination li {
        padding: 8px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 14px;
    }
</style>
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                        @include('Adminlayout.errors')
                            <h4 class="card-title"> رسال تواصل الموقع </h4>
                            @if(count($data) > 0)
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td> رقم الرساله # </td>
                                        <td> الاسم بالكامل </td>
                                        <td> البريد الالكتروني </td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $con)
                                    <tr>
                                    <td>{{ $con->id }}</td>
                                    <td>{{ $con->fname }}  {{ $con->lname }}</td>
                                    <td> {{ $con->email }}</td>
                                    <td> <a href="/admin/watch_contact_massege/{{ $con->id }}" class="btn btn-info btn-sm"> مشاهده </a>
                                    <a onclick="return confirm('Are you sure?')" href="/admin/delete_contact_massege/{{ $con->id }}" class="btn btn-danger btn-sm"> حذف </a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $data->links() }}
                            @else
                            <div class="alert alert-success text-center">
                                <p><strong> لا توجد بيانات ف الوقت الحالي </strong></p>
                            </div>
                            @endif

                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
