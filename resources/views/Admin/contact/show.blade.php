@extends('Adminlayout.master')
@section('title', 'اتصل بنا')

@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                            <li class="breadcrumb-item active"> <a href="/admin/contact_us">اعدادات الاتصال </a> </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                                
                            <div class="card-header bg-danger form-group">
                                <h4 class="m-b-0 text-white">رسال تواصل الموقع</h4>
                            </div>

                                    <div class="form-group row">
                                <div class="col-md-4 form-group"> 
                                    <label >الاسم بالكامل</label>  
                                    <input readonly class="form-control" value="{{ $data->fname }} {{ $data->lname }}">
                                </div>
                                <div class="col-md-4 form-group">
                                    <label> البريد الالكتروني </label>
                                    <input readonly class="form-control" value="{{ $data->email }}">
                                </div>
                                <div class="col-md-4 form-group">
                                    <label>العنوان</label>
                                    <input readonly class="form-control" value="{{ $data->address }}">                                            
                                </div>
                                <br/>
                                    <div class="col-md-12 form-group">
                                        <label> الرساله </label>
                                        <textarea readonly  rows="5" class="form-control">{{ $data->massege }}</textarea>
                                    </div>
                                    <br/>
                                    <div class=" pull-right">
                                            <b> تاريخ الاستقبال </b> :  <small> {{ $data->created_at->format('Y-m-d') }} </small>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>


        </div>
    </div>
</div>
@endsection