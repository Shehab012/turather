@extends('Adminlayout.master')
@section('title', 'القوائم الرائسيه')
@section('content')
<style>
    .pagination li {
        padding: 8px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 14px;
    }
</style>    
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">االقوائم الرائسيه </h4>
                        @include('Adminlayout.errors')
                        <div class="pull-left form-group">
		                        <a href="/admin/categoryAdd" class="btn btn-success"> <span class="fa fa-plus"></span> اضافة قسم رائيسي جديد </a>
                        </div>
                            <table class="table table-striped text-center">
                            <thead>
                                	<tr>
                                		<th>الرقم</th>
                                		<th>الترتيب</th>
                                		<th>الاسم بالعربي</th>
                                		<th>الاسم بالانجليزي</th>
                                		<th>مفعل</th>
                                		<th>عدد الاقسام الفرعيه</th>
                                		<th></th>
                                	</tr>
                            </thead>
                            <tbody>
                            @foreach($data as $main)
                                <tr>
                                    <td> {{ $main->id }} </td>
                                    <td> <span class="btn waves-effect waves-light btn-rounded btn-info btn-sm">{{ $main->order }}</span></td>
                                    <td> <a href="/admin/main_cat_show/{{ $main->id }}"> {{ $main->cat_name_ar }} </a></td>

                                    <td> {{ $main->cat_name_en }} </td>
                                    @if(  $main->status  == 1 )
                                        <td>
                                            <span class="btn btn-info btn-sm"> نعم</span>
                                        </td>
                                    @else
                                        <td>
                                            <span class="btn btn-danger btn-sm"> لا  </span>
                                        </td>
                                    @endif
                                    <?php
                                    $sub = \App\sub_cat::where('main_cat_id',$main->id)->count();
                                    ?>                                    
                                    <td> {{ $sub }} </td>

                                    <td>
                                    <a href="/admin/update_main_caret/{{ $main->id }}" class=" btn btn-info btn-sm">  <span class="fa fa-edit"></span>  تعديل</a>
                                    
                                    <a onclick="return confirm('Are you sure?')" href="/admin/delete_main_caret/{{ $main->id }}" class=" btn btn-danger btn-sm"> <span class="fa fa-trash"></span>  حذف</a>
                                    
                                </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $data->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection