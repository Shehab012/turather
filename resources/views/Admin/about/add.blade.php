@extends('Adminlayout.master')
@section('header')
@section('title', 'من نحن')

<link href="{{url('/back')}}/dist/css/pages/tab-page.css" rel="stylesheet">
<link href="{{url('/back')}}/dist/css/pages/breadcrumb-page.css" rel="stylesheet">
<link href="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />

@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="#">اعدادات الموقع</a></li>
                            <li class="breadcrumb-item active">الاعدادات الرئيسية</li>
                        </ol>
                    </div>
                </div>
            </div>

            <?php
            $about = \App\about_us::first();
            ?> 
            @if(!$about)
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @include('Adminlayout.errors')
                            <form class="form-group" method="POST" enctype="multipart/form-data"  action="/admin/addabout">
                                {{ csrf_field() }}
                                    <div class="card-header bg-danger form-group">
                                        <h4 class="m-b-0 text-white">من نحن</h4>
                                    </div>            
                                <div class="form-group col-md-12">
                                    <div class="row">
                                                <div class="col-md-6 form-group {{ $errors->has('title_ar') ? ' has-danger' : '' }}">
                                                    <label> <b> العنوان عربي </b> </label>
                                                    <input class="form-control" name="title_ar" />
                                                    @if ($errors->has('title_ar'))
                                                        <small class="form-control-feedback">{{ $errors->first('title_ar') }} </small>
                                                    @endif                    
                                                </div>
                                                <div class="col-md-6 form-group {{ $errors->has('title_en') ? ' has-danger' : '' }}">
                                                        <label> <b> العنوان انجليزي </b> </label>
                                            <input class="form-control" name="title_en" />
                                            @if ($errors->has('title_en'))
                                            <small class="form-control-feedback">{{ $errors->first('title_en') }} </small>
                                            @endif                    

                                        </div>  
                                    </div>
                                </div>

                                    <div class="card-header bg-danger form-group">
                                        <h4 class="m-b-0 text-white">الوصف</h4>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="row">
                                            <div class="col-md-12 form-group {{ $errors->has('desc_ar') ? ' has-danger' : '' }}">
                                                <label> <b> الوصف بالعربي </b> </label>
                                                <textarea id="tarea" rows=4 class="form-control" name="desc_ar"> </textarea>
                                                @if ($errors->has('desc_ar'))
                                                <small class="form-control-feedback">{{ $errors->first('desc_ar') }} </small>
                                                @endif                    
                                            </div>
                                            <div class="col-md-12 form-group {{ $errors->has('desc_en') ? ' has-danger' : '' }}">
                                                <label> <b> الوصف بالانجليزي </b> </label>
                                                <textarea id="tarea2" rows=4 class="form-control" name="desc_en"> </textarea>    
                                                @if ($errors->has('desc_en'))
                                                <small class="form-control-feedback">{{ $errors->first('desc_en') }} </small>
                                                @endif                    
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 form-group {{ $errors->has('Image') ? ' has-danger' : '' }}">
                                                <label> <b> الصوره </b> </label>
                                                <input type="file" name="Image" class="form-control">
                                                @if ($errors->has('Image'))
                                                <small class="form-control-feedback">{{ $errors->first('Image') }} </small>
                                                @endif                        
                                            </div>
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="col-md-12 form-group">
                                        <div class="text-left">                                        
                                            <a href="/admin" class="btn btn-danger"> <span class="fa fa-sign-out"> </span> العوده </a>
                                            <button class="btn btn-success" ><span class="fa fa-send"> </span> اضافه</button>
                                        </div>
                                    </div>
        
                                </form>
                        </div>
                    </div>
            </div> 
            @else 
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                        <form class="form-group" method="POST" enctype="multipart/form-data"  action="/admin/editabout/{{ $about->id }}">
                                {{ csrf_field() }}
                                    <div class="card-header bg-danger form-group">
                                        <h4 class="m-b-0 text-white">من نحن</h4>
                                    </div>            
                                <div class="form-group col-md-12">
                                    <div class="row">
                                                <div class="col-md-6 form-group {{ $errors->has('title_ar') ? ' has-danger' : '' }}">
                                                    <label> <b> العنوان عربي </b> </label>
                                                <input class="form-control" name="title_ar" value="{{ $about->title_ar }}" />
                                                    @if ($errors->has('title_ar'))
                                                        <small class="form-control-feedback">{{ $errors->first('title_ar') }} </small>
                                                    @endif                    
                                                </div>
                                                <div class="col-md-6 form-group {{ $errors->has('title_en') ? ' has-danger' : '' }}">
                                                        <label> <b> العنوان انجليزي </b> </label>
                                            <input class="form-control" name="title_en" value="{{ $about->title_en }}" />
                                            @if ($errors->has('title_en'))
                                            <small class="form-control-feedback">{{ $errors->first('title_en') }} </small>
                                            @endif                    

                                        </div>  
                                    </div>
                                </div>

                                    <div class="card-header bg-danger form-group">
                                        <h4 class="m-b-0 text-white">الوصف</h4>
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="row">
                                            <div class="col-md-12 form-group {{ $errors->has('desc_ar') ? ' has-danger' : '' }}">
                                                <label> <b> الوصف بالعربي </b> </label>
                                            <textarea id="tarea" rows=4 class="form-control" name="desc_ar">{{ $about->desc_ar }}</textarea>
                                                @if ($errors->has('desc_ar'))
                                                <small class="form-control-feedback">{{ $errors->first('desc_ar') }} </small>
                                                @endif                    
                                            </div>
                                            <div class="col-md-12 form-group {{ $errors->has('desc_en') ? ' has-danger' : '' }}">
                                                <label> <b> الوصف بالانجليزي </b> </label>
                                                <textarea id="tarea2" rows=4 class="form-control" name="desc_en"> {{ $about->desc_en }} </textarea>    
                                                @if ($errors->has('desc_en'))
                                                <small class="form-control-feedback">{{ $errors->first('desc_en') }} </small>
                                                @endif                    
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 form-group {{ $errors->has('Image') ? ' has-danger' : '' }}">
                                                <label> <b> الصوره </b> </label>
                                                <input type="file" name="Image" class="form-control">
                                                @if ($errors->has('Image'))
                                                <small class="form-control-feedback">{{ $errors->first('Image') }} </small>
                                                @endif                        
                                            </div>
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="col-md-12 form-group">
                                        <div class="text-left">
                                            <div class="pull-left">
                                                <button class="btn btn-success" ><span class="fa fa-send"> </span> اضافه</button>                                                    
                                            </div>
                                            <div class="pull-right">
                                                <a href="/admin" class="btn btn-danger"> <span class="fa fa-sign-out"> </span> العوده </a>
                                            </div>                                        
                                        </div>
                                    </div>
        
                                </form>
                        </div>
                    </div>
            </div>   
            @endif

        </div>
    </div>
</div>
@section('js')

<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'tarea' );
</script>
<script>
    CKEDITOR.replace( 'tarea2' );
</script>
    
@endsection
@endsection