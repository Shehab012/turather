@extends('Adminlayout.master')
@section('title', 'تعديل منتج')

@section('header')
<link href="{{url('/back')}}/dist/css/pages/tab-page.css" rel="stylesheet">
<link href="{{url('/back')}}/dist/css/pages/breadcrumb-page.css" rel="stylesheet">
<link href="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />


@section('content')
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="/admin">اعدادات الموقع</a></li>
                            <li class="breadcrumb-item active"><a href="/admin/product">اعدادات المنتج</a></li>                            
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @include('Adminlayout.errors')
                            <form class="form-group" method="POST" enctype="multipart/form-data"  action="/admin/updateproduct/{{ $data->id }}">
                                {{ csrf_field() }}
                                <div class="card-header bg-danger form-group">
                                    <h4 class="m-b-0 text-white">تعديل اسم المنتج</h4>
                                </div>       
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for=""> اسم المنتج بالعربي  </label>
                                        <input type="text" value="{{ $data->product_title_ar }}" name="product_name_ar" class="form-control">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="">اسم المنتج بالنجليزي</label>
                                        <input type="text" value="{{ $data->product_title_en }}" name="product_name_en" class="form-control">
                                    </div>

                                    <div class="card-header bg-danger col-12 form-group">
                                        <h4 class="m-b-0 text-white">تعديل المنتج</h4>
                                    </div>       
                                        <div class="col-md-6 form-group">
                                            <label> التفعيل </label>
                                            <select name="type" class="form-control">
                                                <option value="1"> تفعيل </option>
                                                <option value="2"> ايقاف </option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for=""> السعر </label>
                                            <input type="number" value="{{ $data->price }}" step=".01" name="price" class="form-control" >
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for=""> الصوره </label>
                                            <input type="file" name="Image" class="form-control">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label for=""> ترتيب المنتج </label>
                                            <input type="number" value="{{ $data->product_order }}" name="order" class="form-control">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <img src="{{ asset('webImage/'.$data->image) }}" class="img-responsive" width="100px" height="100px" />
                                        </div>

                                        <div class="card-header bg-danger col-12 form-group">
                                            <h4 class="m-b-0 text-white">وصف المنتج</h4>
                                        </div>    

                                        <div class="col-md-6 form-group">
                                            <label for="">  الوصف المصغر بالعربي</label>
                                            <input type="text" maxlength="185" value="{{ $data->product_small_desc_ar }}" name="small_ar_desc" class="form-control" >
                                        </div>

                                        <div class="col-md-6 form-group">
                                            <label for="">  الوصف المصغر بالنجليزي</label>
                                            <input type="text" maxlength="185" value="{{ $data->product_small_desc_en }}" name="small_en_desc" class="form-control" >
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label for=""> الوصف بالعربي </label>
                                            <textarea rows="5" id="tarea" class="form-control" name="desc_ar">{{ $data->product_desc_ar }}</textarea>
                                        </div>

                                        <div class="col-md-12 form-group">
                                            <label for=""> الوصف بالنجليزي </label>
                                            <textarea rows="5" id="tarea2" class="form-control" name="desc_en">{{ $data->product_desc_en }}</textarea>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="text-left">
                                                <div class="pull-left">
                                                    <button class="btn btn-success" ><span class="fa fa-send"> </span> تعديل</button>                                                    
                                                </div>             
                                                <div class="pull-right">
                                                    <a href="/admin/product" class="btn btn-danger"> <span class="fa fa-sign-out"> </span> العوده </a>
                                                </div>                         
                                            </div>
                                        </div>                
                                    </div>     
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'tarea' );
    CKEDITOR.replace( 'tarea2' );
</script>
<script src="{{url('/back')}}/assets/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
@endsection

@endsection
