@extends('Adminlayout.master')
@section('title', 'المنتجات')

@section('content')
<style>
    .pagination li {
        padding: 8px;
        font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        font-size: 14px;
    }
</style>            
<div class="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <ol class="breadcrumb m-b-10">
                            <li class="breadcrumb-item"><a style="margin-left: 1px;" href="{{url('/admin')}}">الرئيسية</a></li>
                            <li class="breadcrumb-item"><a href="#">اعدادات الموقع</a></li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"> المنتجات </h4>
                        @include('Adminlayout.errors')
                        <div class="pull-left form-group">
                            <a href="/admin/addproduct" class="btn btn-success"> <span class="fa fa-plus"></span> اضافة منتج </a>
                        </div>
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th> رقم المنتج </th>
                                        <th> اسم المنتج عربي </th>
                                        <th> اسم المنتج انجليزي </th>
                                        <th> سعر المنتج </th>
                                        <th>  </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($products as $data)
                                        <tr>
                                            <td>{{ $data->id }}</td>
                                            <td>{{ $data->product_title_ar }}</td>
                                            <td>{{ $data->product_title_en }}</td>
                                            <td>{{ $data->price }} ج </td>
                                            <td>
                                                <a href="/admin/updateproduct/{{ $data->id }}" class="btn btn-info btn-sm">تعديل</a>
                                                <a onclick="return confirm('Are you sure?')" href="/admin/deleteproduct/{{ $data->id }}" class="btn btn-danger btn-sm" >حذف</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $products->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
