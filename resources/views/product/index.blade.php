@extends('Indexlayout.master')
@section('content')
<section class="sec-pd">
    <div class="container">

        <div class="row">
            <div class="col-md-8">
                <div class="post sub-sec pro">
                    <div class="row">
                        @foreach($products as $product)
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="products text-center">
                                    <div class="sec-image">
                                        
                                    @if(Session::get('lang') == 'ar')
                                        <a href="/product/{{ $product->product_slogan_ar}}"> <img src="{{ asset('webImage/'.$product->image) }}" class="img-responsive" /></a>
                                    @elseif(Session::get('lang') == 'en')
                                        <a href="/product/{{ $product->product_slogan_ar}}"> <img src="{{ asset('webImage/'.$product->image) }}" class="img-responsive" /></a>
                                    @endif
                                    </div>
                                    @if(Session::get('lang') == 'ar')
                                        <h3><a href="/product/{{ $product->product_slogan_ar}} "> {{ $product->product_title_ar }} </a></h3>
                                    @elseif(Session::get('lang') == 'en')
                                    <h3><a href="/product/{{ $product->product_slogan_en }}"> {{ $product->product_title_en }} </a></h3>                                    
                                    @endif
                                    @if(Session::get('lang') == 'ar')
                                        <h3 class="price">{{ $product->price }} ج  </h3>                                    
                                        <p> {{ $product->product_small_desc_ar }} </p>
                                        <a href="/product/{{ $product->product_slogan_ar }}" class="btn btn-success"> @lang('home.ReadMore') <i class="fas fa-angle-double-left"></i>
                                    @elseif(Session::get('lang') == 'en')
                                    <h3 class="price">{{ $product->price }} .LE  </h3>                                    
                                        <p> {{ $product->product_small_desc_en }} </p>
                                        <a href="/product/{{ $product->product_slogan_en }}" class="btn btn-success"> @lang('home.ReadMore') <i class="fas fa-angle-double-left"></i>                                        
                                    @endif
                                    </a>
                                </div>
                            </div>
                        @endforeach
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
            @include('Indexlayout.sidebar')
        </div>
    </div>
</section>

@endsection