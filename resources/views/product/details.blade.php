@extends('Indexlayout.master')
@section('content')
<section class="sec-pd contentpage">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="post sub-sec">
                        <ol class="breadcrumb">
                            <li><a href="#"> <i class="fas fa-home"></i> @lang('home.home')</a></li>
                            <li class="active">@lang('home.productName')</li>
                        </ol>
                        <div class="pro-top">
                            @if(Session::get('lang') == 'ar')
                                <h1 class="pg-title pull-right"><i class="fas fa-tags"></i> {{ $product->product_title_ar }}</h1>
                                <h1 class="price pull-left"> {{ $product->price }} ج</h1>                                
                            @elseif(Session::get('lang') == 'en')
                                <h1 class="pg-title pull-right"><i class="fas fa-tags"></i> {{ $product->product_title_en }}</h1>
                                <h1 class="price pull-left"> {{ $product->price }} .LE</h1>                                
                            @endif                            
                        </div>
                        <div class="clearfix"></div>
                        @if(Session::get('lang') == 'ar')
                            <p> {{ $product->product_small_desc_ar }} </p>
                        @elseif(Session::get('lang') == 'en')
                            <p> {{ $product->product_small_desc_en }} </p>
                        @endif
                        <hr>
                        <div class="sec-image">
                            <img src="{{ asset('webImage/'.$product->image) }}" class="img-responsive" />
                        </div>
                        @if(Session::get('lang') == 'ar')
                            <p> {!! $product->product_desc_ar !!} </p>
                        @elseif(Session::get('lang') == 'en')
                            <p> {!! $product->product_desc_en !!} </p>
                        @endif
                        <a class="btn btn-success" href="#">@lang('home.productRequestNow')</a> 
                            <hr>                        
                    </div> 
                    <div class="post sub-sec" >
                        <div class="comment">
                                <div class="sec-title">
                                    <h3>@lang('home.productRequest')</h3>
                                </div>
                                    <hr>
                                    @include('Adminlayout.errors')
                                <form enctype="multipart/form-data" action="/productRequest" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                            <input type="hidden" name="product_id" value="{{ $product->id }}" class="form-control">                                            
                                        <div class="form-group col-md-6">
                                            <input type="text" name="fname" class="form-control" placeholder="@lang('home.First_Name')" />
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="text" name="lname" class="form-control" placeholder="@lang('home.Last_Name')" />
                                        </div>
                                        <div class="form-group col-md-6 ">
                                            <input type="email" name="email" class="form-control" placeholder="@lang('home.Enteryouremailaddress')" />
                                        </div>
                                        <div class="form-group col-md-6 ">
                                            <input type="tel" name="phone" class="form-control" placeholder="@lang('home.Mobile')" />
                                        </div>
                                        <div class="form-group col-md-12">
                                                <textarea class="form-control" name="note" placeholder="@lang('home.Note')"></textarea>
                                            </div>
    
                                        <div class="form-group group-btn-tours col-xs-12 ">
                                            <button type="sumbit" class="btn btn-success">@lang('home.Send')</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>   
                </div>                 
                <div>
                    @include('Indexlayout.sidebar')
                </div>
            </div>
        </div>
</section>
@endsection