<div class="col-md-4 sticky">
        <div class="sidebar ">
            <div class="side-info">
                <div class="sec-title">
                    <h4>@lang('home.Stayintouchwithus')</h4>
                </div>
                <hr>
                <?php
                    $social = \App\setting::first();
                ?>
                <ul class="list-inline social side-pad">
                    @if(!empty($social->website_facebook))
                        <li class="fc"><a target="_blank" href="{{ $social->website_facebook }}"><i class="fab fa-facebook-f"></i></a></li>
                    @endif
                    @if(!empty($social->website_twetter))
                        <li class="tw"><a target="_blank" href="{{ $social->website_twetter }}"><i class="fab fa-twitter"></i></a></li>
                    @endif
                    @if(!empty($social->website_youtube))
                        <li class="yo"><a target="_blank" href="{{ $social->website_youtube  }}"><i class="fab fa-youtube"></i></a></li>
                    @endif
                    @if(!empty($social->website_instagram))
                        <li class="in"><a target="_blank" href="{{ $social->website_instagram }}"><i class="fab fa-instagram"></i></a></li>
                    @endif
                    @if(!empty($social->website_google))
                    <li class="go"><a target="_blank" href="{{ $social->website_google }}"><i class="fab fa-google-plus-g"></i></a></li>
                    @endif
                    {{-- <li class="go"><a target="_blank" href="#"><i class="fab fa-linkedin"></i></a></li>                     --}}
                </ul>
            </div>
        </div>
        <div class="side-info">
                <div class="sec-title">
                    <h4>{{ trans('home.Mostviews') }} </h4>
                </div>
                <hr>
                <?php
                $last_i = \App\item::where('status','1')->orderBy('viewers','desc')->limit(1)->get();
                ?>
                <div class="last-news">
                    @foreach($last_i as $i)
                        <div class=" sub-sec">
                            <div class="sec-image">
                            <img src="{{asset('webImage/'.$i->image)}}" class="img-responsive"/>

                            </div>
                            <div class="info-top">
                                <span>
                                <i class="far fa-clock"></i> {{ $i->created_at->diffForHumans() }}</span>
                                <span class="pull-left">
                                        <?php
                                            $i_comment = \App\comment::where('item_id',$i->id)
                                                                      ->where('status','active')->get();
                                        ?>
                                        @if(count($i_comment) > 0)
                                            <i class="fas fa-comments"> </i>  {{ count($i_comment) }}
                                        @endif
                                </span>
                            </div>
                            @if(Session::get('lang')=='ar')
                                <h4> <a href="/post/{{$i->subcat_slogen_ar}}">{{ $i->title_ar }}</a></h4>
                                <p> {{ $i->desc_ar_mini }} </p>
                            @endif

                            @if(Session::get('lang')=='en')   
                                <h4> <a href="/post/{{$i->subcat_slogen_en}}">{{ $i->title_en }}</a></h4>
                                <p> {{ $i->desc_en_mini }} </p>  
                            @endif
                        </div>
                        @endforeach
                        <hr>
                        <?php
                            $all_items = \App\item::where('status','1')->orderBy('viewers','desc')->limit(4)->get();
                        ?> 
                            @foreach($all_items as $all)
                                <div class="tr-submain">                                            
                                    <div class="tr-img">
                                        <a href="#">
                                        <img src="{{asset('webImage/'.$all->image)}}" class="/img-responsive" height="150px" width="200px"  />
                                        </a>
                                    </div>
                                    <div class="tr-info">
                                        <div class="info-main">
                                            <h4>
                                            @if(Session::get('lang')=='ar')    
                                                <a href="/post/{{ $all->subcat_slogen_ar }}">{{ $all->title_ar }}</a>
                                            @endif

                                            @if(Session::get('lang')=='en')    
                                                <a href="/post/{{ $all->subcat_slogen_en }}">{{ $all->title_en }}</a>
                                            @endif

                                            </h4>
                                        </div>
                                    <span><i class="far fa-clock"></i>{{ $all->created_at->diffForHumans() }}</span>
                                    <!--<div>-->
                                    <!--    @if(Session::get('lang')=='ar')-->
                                    <!--        {{ $all->desc_ar_mini }}-->
                                    <!--    @endif-->

                                    <!--    @if(Session::get('lang')=='en')-->
                                    <!--        {{ $all->desc_en_mini }}-->
                                    <!--    @endif-->
                                    <!--</div>-->
                                    </div>
        
                                </div>                                            
                            @endforeach
                        </div>
                    </div>
                     <div class="side-info">
                            <div class="sec-title">
                                <h4>اشهر الاخبار</h4>
                            </div>
                            <hr>
                            <?php
                            $products = \App\product::orderBy('viewers','Desc')->limit(5)->get();
                            $i = 1;
                            ?>
                                <div class="popular-news">
                                    @foreach($products as $product)
                                    <div class="tr-submain">
                                        <div class="tr-img">
                                            <a href="#">
                                                <img src="{{ asset('webImage/'.$product->image) }}" class="img-responsive" />
                                            </a>
                                        </div>
                                            <span class="num-p">{{ $i++ }} </span>
                                        <div class="tr-info">
                                            <div class="info-main">
                                                <h4>
                                                    @if(Session::get('lang') == 'ar')
                                                        <a href="/product/{{ $product->product_slogan_ar}}">{{ $product->product_title_ar }}</a>
                                                    @elseif(Session::get('lang') == 'en')
                                                        <a href="/product/{{ $product->product_slogan_en }}">{{ $product->product_title_en }}</a>
                                                    @endif
                                                </h4>
                                            </div>
                                            <span><i class="far fa-clock"></i>{{ $product->updated_at->format('Y-m-d') }}</span>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                </div>
