<!DOCTYPE html>
<html lang="en">
    <!-- start header   -->
<?php
  $setting = \App\setting::first();
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="{{$setting->website_desc_ar}} {{ $setting->website_desc_en }}">
<meta name="keywords" content="{{$setting->website_keywor_ar}} {{ $setting->website_keywor_en }} "> 
@if($setting->website_icon)
    <link rel="icon" type="image/png"  href="{{ asset('webImage/'.$setting->website_icon) }}">
@endif

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    @if(Session::get('lang')=='en')
        @if($setting->website_name_en)
            <title>{{ $setting->website_name_en }}</title>
        @endif
    @endif
    
    @if(Session::get('lang')=='ar')
        @if($setting->website_name_ar)
            <title>{{ $setting->website_name_ar }}</title>
        @endif
    @endif

    <!-- Bootstrap -->
    <link rel="stylesheet" href=" {{ asset('css/bootstrap.min.css') }}">
    <!-- bootstrap ar  -->
    @if(Session::get('lang') == 'ar')
    <link rel="stylesheet" href=" {{ asset('css/bootstrap-rtl.min.css') }}">    
    @endif

    
    <link rel="stylesheet" href=" {{ asset('css/animate.min.css') }} ">
    <link rel="stylesheet" href=" {{ asset('inc/carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href=" {{ asset('inc/carousel/owl.theme.default.css') }}">
    <link rel="stylesheet" href=" {{ asset('inc/carousel/owl.theme.green.min.css') }} ">
    <link rel="stylesheet" href=" {{ asset('https://use.fontawesome.com/releases/v5.0.13/css/all.css') }} ">
    <link rel="stylesheet" href=" {{ asset('css/style.css') }} ">
    

    @if(Session::get('lang') == 'en')
    <link rel="stylesheet" href=" {{ asset('css/style-en.css') }} ">
    @endif


    

    <!-- <link rel="stylesheet" href="css/style-en.css"> -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>