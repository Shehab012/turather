
</div>
<footer id="footer">
    <div class="footer-top" >
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="col-12">
                       <div class="ft-logo">
                            @foreach($settings as $s)
                            
                                @if(Session::get('lang') == 'ar')
                                    @if(!empty($s->website_logo_footer_ar))
                                        <img src="{{ asset('webImage/'.$s->website_logo_footer_ar) }}" width="100px" height="100px" class="img-responsive" />
                                    @endif
                                
                                @elseif(Session::get('lang')=='en')
                                    @if(!empty($s->website_logo_footer_en))
                                        <img src="{{ asset('webImage/'.$s->website_logo_footer_en) }}" width="100px" height="100px" class="img-responsive" />
                                    @endif
                                @endif
                        </div>
                        <div class="col-md-12">
                            <p> 
                                @if(Session::get('lang')=='ar')
                                    @if(!empty( $s->website_about_ar))
                                        <p>{{ $s->website_about_ar }}</p>
                                    @endif
                                @endif
                        
                                @if(Session::get('lang')=='en')
                                    @if(!empty($s->website_about_en))
                                        <p>{{ $s->website_about_en }}</p>
                                    @endif
                                @endif
                            </p>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-2 col-sm-2">
                        <div class="sec-title">
                            <h4>@lang('home.Sections')</h4>
                        </div>      
                        <?php
                            $sub_cat = \App\sub_cat::where('status','1')->inRandomOrder()->limit(4)->get();
                        ?>
                        <ul class="list-unstyled">
                            @foreach($sub_cat as $d)
                                <li>
                                    @if(Session::get('lang')=='ar')
                                        @if(!empty($d->sub_cat_name_ar))
                                            <a href="#"> {{ $d->sub_cat_name_ar }} </a>
                                        @endif
                                    @endif
                                    
                                    @if(Session::get('lang')=='en')
                                        @if(!empty($d->sub_cat_name_en))
                                            <a href="#"> {{ $d->sub_cat_name_en }} </a>
                                        @endif
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                <div class="col-md-2 col-sm-2">
                            <div class="sec-title">
                                <h4>@lang('home.ImportantLinks')</h4>
                            </div>
                            
                            <ul class="list-unstyled">
                                <li>
                                    <a href="#">@lang('home.Connectwithus')</a>
                                </li>
                                <li>
                                    <a href="#">@lang('home.Aboutthesite') </a>
                                </li>
                            </ul>
    
                        </div>
                <div class="col-md-4 col-sm-4">
                                <div class="sec-title">
                                    <h4>@lang('home.Stayintouchwithus')</h4>
                                </div>
                                    <?php
                                        $social = \App\setting::first();
                                    ?>
        
                                <ul class="list-inline social">
                                    @if(!empty($social->website_facebook))
                                        <li class="fc"><a target="_blank" href="{{ $social->website_facebook }}"><i class="fab fa-facebook-f"></i></a></li>
                                    @endif
                                    @if(!empty($social->website_twetter))
                                        <li class="tw"><a target="_blank" href="{{ $social->website_twetter }}"><i class="fab fa-twitter"></i></a></li>
                                    @endif
                                    @if(!empty($social->website_youtube))
                                        <li class="yo"><a target="_blank" href="{{ $social->website_youtube }}"><i class="fab fa-youtube"></i></a></li>
                                    @endif
                                    @if(!empty($social->website_instagram))
                                        <li class="in"><a target="_blank" href="{{ $social->website_instagram }}"><i class="fab fa-instagram"></i></a></li>
                                    @endif
                                    @if(!empty($social->website_google))
                                        <li class="go"><a target="_blank" href="{{ $social->website_google }}"><i class="fab fa-google-plus-g"></i></a></li>
                                    @endif
                                    {{-- <li class="go"><a href="{{ $social->website_linked_in }}"><i class="fab fa-linkedin"></i></a></li> --}}
                                </ul>
                        </div>
            </div>
        </div>
    </div>
</footer>
    
    
<!-- end footer -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('inc/carousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

</body>

</html>