<body>
    <!-- start header   -->
    <?php
        $setting = \App\setting::first();
    ?>
    <div class="body-all">
        <div class="imgtop container">
            {{-- <img src="/storage/images/{{ $setting->website_logo_ar }}" class="img-responsive"/> --}}
        </div>

        <div class="imgtop container">
                <img src="{{ asset('images/banniere monuments.png') }}" class="img-responsive"/>
            </div>  
    
        <header class="header_area">
            <div class="main_header_area animated">
                <div class="container">

                    <nav id="navigation1" class="navigation">
                        <!-- Logo Area Start -->
                        <div class="nav-header">
                          
                            <div class="nav-toggle"></div>
                        </div>
                        <!-- Search panel Start -->
                        
                        <!-- Main Menus Wrapper -->
                        <div class="nav-menus-wrapper">
                            <ul class="nav-menu ">
                                <li>
                                <a href="/">{{trans('home.home')}}</a>
                                </li>
                                <li>                                        
                                    <a href="/about">@lang('home.Aboutthesite')</a> 
                                </li>

                                @foreach($data as $nav)
                                    @if( Session::get('lang') == 'ar' )
                                        <li>
                                            <a href="#">{{ $nav->cat_name_ar }}</a>
                                            <div class="megamenu-panel">
                                                <div class="megamenu-lists">
                                                    <?php
                                                        $sub_cat = \App\sub_cat::where('main_cat_id',$nav->id)
                                                                                ->where('status','1')->get();
                                                        ?>
                                                    @foreach($sub_cat as $sub)
                                                    <ul class="megamenu-list list-col-4">
                                                        <li class="megamenu-list-title">
                                                            <a href="#">{{ $sub->sub_cat_name_ar }}</a>
                                                        </li>
                                                        <?php
                                                            $items = \App\subofsub::where('sub_cat_id',$sub->id)
                                                                               ->where('status','1')->get();
                                                            ?>
                                                            @foreach($items as $item)
                                                            <li>
                                                                <a href="/posts/{{ $item->s_s_slogan_ar }}" >{{ $item->sub_of_sub_name_ar }}</a>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </li>
                                        @endif

                                        @if(Session::get('lang') == 'en')
                                            <li>
                                                <a href="#">{{ $nav->cat_name_en }}</a>
                                                <div class="megamenu-panel">
                                                    <div class="megamenu-lists">
                                                        <?php
                                                            $sub_cat = \App\sub_cat::where('main_cat_id',$nav->id)->get();
                                                            ?>
                                                        @foreach($sub_cat as $sub)
                                                        <ul class="megamenu-list list-col-4">
                                                            <li class="megamenu-list-title">
                                                                <a href="/posts/{{ $sub->subcat_slogen_en }}">{{ $sub->sub_cat_name_en }}</a>
                                                            </li>
                                                            <?php
                                                                $items = \App\subofsub::where('sub_cat_id',$sub->id)->get();
                                                                ?>
                                                                @foreach($items as $item)
                                                                <li>
                                                                    <a href="/post/{{ $item->s_s_slogan_en }}" >{{ $item->sub_of_sub_name_en }}</a>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                            @endforeach
                                                    </div>
                                                </div>
                                            </li>
                                        @endif
                                    @endforeach
                                <li>
                                    <a href="/contact">{{ trans('home.Connectwithus') }} </a>
                                </li>
                                <li>
                                    <a href="/products">{{ trans('home.Theshope') }} </a>
                                </li>
                                <li>
                                    @if(Session::get('lang') == 'ar')
                                        <a href="/lang/en">English</a>
                                    @elseif(Session::get('lang')=='en')
                                        <a href="/lang/ar">العربي</a>
                                    @endif
                                </li>

                            </ul>
                        </div>
                        <div class="nav-search">
                                <div class="nav-search-button">
                                    <i class="nav-search-icon"></i>
                                </div>
                                <form action="/search" role="search"  method="GET">
                                    <div class="nav-search-inner">
                                        @if(Session::get('lang') == 'ar')
                                            <input type="text"  name="search" placeholder="ابحث">
                                        @elseif(Session::get('lang') == 'en')
                                            <input type="text"  name="search" placeholder="Type Your Keywords">
                                        @endif
                                    </div>
                                </form>
                            </div>
                    </nav>
                </div>
            </div>
        </header>
        
        <!-- end  header   -->
