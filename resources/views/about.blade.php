@extends('Indexlayout.master')
@section('content')
<section class="sec-pd">
    
    @if(!$about)
    <div class="container">
            <div class="post sub-sec">
                <div class="">
                        <ol class="breadcrumb">
                            <li><a href="/"> <i class="fas fa-home"></i>@lang('home.home')</a></li>
                            <li><a href="#">@lang('home.AboutUs')</a></li>
                         </ol>
                </div>
<hr>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                            <div class="sec-image">
                            <img src="{{ asset('images/post_1.jpg') }}" height="445px" width="626px" class="img-responsive" />
            
                                </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <h1> @lang('home.Address') ! </h1>
                        <p>@lang('home.TheMessage')</p>
                    </div>
                    
            </div>
        </div>
    </div>

    @else
        <div class="container">
                <div class="post sub-sec">
                    <div class="">
                            <ol class="breadcrumb">
                                <li><a href="/"> <i class="fas fa-home"></i>@lang('home.home')</a></li>
                                <li><a href="#">@lang('home.AboutUs')</a></li>
                             </ol>
                    </div>
                    @if(Session::get('lang') == 'ar' )    
                        <h1> {{ $about->title_ar }}  </h1>
                    @elseif(Session::get('lang') == 'en')
                        <h1> {{ $about->title_en }} ! </h1>
                    @endif
    <hr>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                                <div class="sec-image">
                                <img src="{{asset('webImage/'.$about->image)}}" height="445px" width="626px" class="img-responsive" />
                
                                    </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            @if(Session::get('lang') == 'ar' )    
                                <p>{!! $about->desc_ar !!}</p>
                                
                            @elseif(Session::get('lang') == 'en')
                                <p>{!! $about->desc_en !!}</p>
                            
                            @endif
                        </div>
                        
                </div>
            </div>
        </div>
    @endif
    
</section>
    
@endsection