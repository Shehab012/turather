@extends('Indexlayout.master')
@section('content')
<div class="container">
<section class="sec-slider sec-pd">
        <div class="owl-carousel owl-theme owl-slider"> 
            @foreach( $item as $i )
            <div class="item">
                @if(Session::get('lang')=='ar')
                    <a href="/post/{{ $i->subcat_slogen_ar }}">
                @endif
                @if(Session::get('lang')=='en')
                <a href="/post/{{ $i->subcat_slogen_en }}">                    
                @endif
                    <img src="{{asset('webImage/'.$i->image)}}" class="img-responsive">
                    <div class="info-overlay">
                        <div class="item-info">
                            <span class="item-date">
                                <i class="fas fa-clock"></i> {{ $i->created_at->format('Y-m') }}
                            </span>
                            @if(Session::get('lang')=='ar')                            
                                <h3>{{ $i->title_ar }}</h3>
                                <p>{{ $i->desc_ar_mini }}</p>
                            @endif
                            @if(Session::get('lang')=='en')
                                <h3>{{ $i->title_en }}</h3>
                                <p>{{ $i->desc_en_mini }}</p>
                            @endif                            
                        </div>
                    </div>
                </a>
            </div>

            @endforeach
            </div>
        </section>
        </div>
        
        
    <section class="content-body sec-pd">
        <div class="container">
                <div class="row">
                    <div class="col-md-8">
            <?php
            $last_item = \App\item::orderBy('created_at','desc')->limit('2')->get();
            ?>
            @foreach($last_item as $last)
                        <div class=" sub-sec">
                            @if(Session::get('lang')=='ar')
                                <h4 class="sec-title">{{ $last->title_ar }}</h4>
                            @endif
                            @if(Session::get('lang')=='en')
                                <h4 class="sec-title">{{ $last->title_en }}</h4>                     
                            @endif
                            <hr>
                            <div class="sec-image">
                                @if(Session::get('lang')=='ar')
                                    <a href="/post/{{ $last->subcat_slogen_ar }}"> <img src="{{asset('webImage/'.$last->image)}}" class="img-responsive" /></a>
                                @elseif(Session::get('lang')=='en')
                                        <a href="/post/{{$last->subcat_slogen_en}}"> <img src="{{asset('webImage/'.$last->image)}}" class="img-responsive" /></a>
                                @endif
                                <div class="post-info sec-info">
                                    <div class="info-top">
                                        <span>
                                            <i class="far fa-clock"></i>
                                            {{ $last->created_at->diffForHumans() }}</span>
                                        <span class="pull-left">
                                            <a href="#">
                                                <?php
                                                    $i_comment = \App\comment::where('item_id',$last->id)
                                                                              ->where('status','active')->get();
                                                ?>
                                                @if(count($i_comment) > 0)
                                                    <i class="fas fa-comments"> </i>  {{ count($i_comment) }}</a>
                                                @endif
                                        </span>
                                    </div>
                                    @if(Session::get('lang')=='ar')                                    
                                        <h2> <a href="/post/{{ $last->subcat_slogen_ar }}">{{ $last->title_ar }}</a></h2>
                                    @endif
                                    @if(Session::get('lang')=='en')                                    
                                        <h2> <a href="/post/{{ $last->subcat_slogen_en }}">{{ $last->title_en }}</a></h2>
                                    @endif


                                </div>
                            </div>

                            @if(Session::get('lang')=='ar')
                                <p> {{ $last->desc_ar_mini }} </p>
                            @endif

                            @if(Session::get('lang')=='en')
                                <p> {{ $last->desc_en_mini }} </p>
                            @endif
                            
                            @if(Session::get('lang')=='ar')
                            <a href="/post/{{ $last->subcat_slogen_ar }}" class="btn btn-success"> {{ trans('home.ReadMore') }} <i class="fas fa-angle-double-left"></i>
                            </a>
                            @endif
                            @if(Session::get('lang')=='en')
                            <a href="/post/{{ $last->subcat_slogen_en }}" class="btn btn-success"> {{ trans('home.ReadMore') }} <i class="fas fa-angle-double-left"></i>
                            </a>
                            @endif
                            
                        </div>
            @endforeach                        
                    </div>
                    @include('Indexlayout.sidebar');
                
                   <!-- start slider -->
                    <!-- end slider -->
            </div>
             <section class="news-slider">
                <h4 class="sec-title">{{ trans('home.WeChoseYou') }}</h4>
                <div class="owl-carousel owl-theme owl-news">
                            <?php
                            $itemrand = \App\item::inRandomOrder()->limit(4)->get();
                            ?>
                            @foreach($itemrand as $rand)
                    <div class="item">
                        <div class="sub-sec">
                            <div class="sec-image">
                            @if(Session::get('lang') == 'ar')
                                <a href="{{ $rand->subcat_slogen_ar}}"> <img src="{{asset('webImage/'.$rand->image)}}" class="img-responsive" /></a>
                            @elseif(Session::get('lang') == 'en')
                                <a href="{{$rand->subcat_slogen_en}}"> <img src="{{asset('webImage/'.$rand->image)}}" class="img-responsive" /></a>
                            @endif

                            </div>
                            <div class="info-top">
                                <span>
                                <i class="far fa-clock"></i>  {{ $rand->created_at->format('Y-m-d') }}</span>
                                <span class="pull-left">
                                    <?php
                                        $i_comment = \App\comment::where('item_id',$rand->id)
                                                                  ->where('status','active')->get();
                                    ?>
                                    @if(count($i_comment) > 0)
                                        <i class="fas fa-comments"> </i>  {{ count($i_comment) }}</a>
                                    @endif

                                    <a href="#">
                                </span>
                            </div>
                            @if(Session::get('lang')=='ar')                            
                                <h3> <a href="/post/{{ $rand->subcat_slogen_ar }}">{{ $rand->title_ar }}</a></h3>
                            @endif
                            @if(Session::get('lang')=='en')                            
                                <h3> <a href="/post/{{ $rand->subcat_slogen_en }}">{{ $rand->title_en }}</a></h3>
                            @endif
                            @if(Session::get('lang')=='ar')                            
                            <p>{{ $rand->desc_ar_mini }}</p>
                            @endif
                            @if(Session::get('lang')=='en')                            
                            <p>{{ $rand->desc_en_mini }}</p>
                            @endif
                            @if(Session::get('lang')=='ar')
                                <a href="/post/{{ $rand->subcat_slogen_ar }}" class="btn btn-success"> {{ trans('home.ReadMore') }} <i class="fas fa-angle-double-left"></i>
                            @elseif(Session::get('lang')=='en')
                                <a href="/post/{{ $rand->subcat_slogen_en }}" class="btn btn-success"> {{ trans('home.ReadMore') }} <i class="fas fa-angle-double-left"></i>
                            @endif
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>


@endsection