@extends('Indexlayout.master')
@section('content')

<section class="sec-pd">
    <div class="container">
        <div class="post sub-sec">
           
            <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                    <div class="">
                        <ol class="breadcrumb">
                            <li><a href="#"> <i class="fas fa-home"></i>@lang('home.home')</a></li>
                            <li><a href="#">@lang('home.Connectwithus')</a></li>
                          </ol>
                        <h3 class="#">تواصل معنا </h3>
                    </div>
                     <hr>
                        <div class="contact-form">
                            @include('Adminlayout.errors')
                                <form method="POST" action="/admin/sendcontact">
                                    {{ csrf_field() }}
                                        <div class="col-md-6 form-group ">
                                            <input type="text" name="fname" class="form-control" placeholder="@lang('home.First_Name')" />
                                        </div>

                                        <div class="col-md-6 form-group ">
                                            <input type="text" name="lname" class="form-control" placeholder="@lang('home.Last_Name')" />
                                        </div>

                                        <div class="col-md-6 form-group ">
                                            <input type="text" name="email" class="form-control" placeholder="@lang('home.Enteryouremailaddress')" />

                                        </div>
                                        <div class="col-md-6 form-group ">
                                                <input type="text" name="address" class="form-control" placeholder="@lang('home.Address')" />
                                        </div>
                                        <div class="col-md-12 form-group ">
                                            <textarea class="form-control" name="massege" rows="5" placeholder="@lang('home.TheMessage')"></textarea>
                                        </div>
                                        
                                        <div class="form-group col-md-3 col-xs-12 ">
                                            <button type="sumbit" class="btn btn-success btn-lg">ارسال</button>
                                        </div>
                                    </form>
                                    <div class="clearfix"></div>
                           </div>
                       </div>
                   </div>
            </div>
        </div>
    </div>
</section>

@endsection