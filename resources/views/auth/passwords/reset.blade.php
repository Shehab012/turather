@extends('frontend.layouts.layout')
@section('title')
استعادة كلمة المرور
@endsection
@section('content')
<div class="page-content">
    <div class="top-page">
        <h1>عن سلزات </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{'/'}}">الرئيسية</a>
            </li>
            <li class="active">استعادة كلمة المرور</li>
        </ol>
    </div>
    <div class="about">
        <div class="container">
            <div class="all">
                <div class="text-center">
                    <h1>استعادة كلمة المرور</h1>

                </div>
                <div class="row">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form  role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div style="   " class="col-md-12{{ $errors->has('email') ? ' has-error' : '' }}"> 
                            <div class="form-group  has-feedback">
                                <label class="control-label" for="p1">البريد الألكترونى</label>
                                <span class="glyphicon glyphicon-envelope form-control-feedback " aria-hidden="true"></span>

                                <input type="text" class="form-control" name="email" value="{{ $email or old('email') }}">
                            </div>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div style="   " class="col-md-12{{ $errors->has('password') ? ' has-error' : '' }}"> 
                            <div class="form-group  has-feedback">
                                <label class="control-label" for="p1">كلمة المرور</label>
                                <span class="glyphicon glyphicon-lock form-control-feedback " aria-hidden="true"></span>

                                <input type="password" class="form-control" name="password" >
                            </div>
                            @if ($errors->has('passworded'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div style="   " class="col-md-12{{ $errors->has('password_confirmation') ? ' has-error' : '' }}"> 
                            <div class="form-group  has-feedback">
                                <label class="control-label" for="p1">تأكيد كلمة المرور</label>
                                <span class="glyphicon glyphicon-lock form-control-feedback " aria-hidden="true"></span>

                                <input type="password" class="form-control" name="password_confirmation" >
                            </div>
                            @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <button class="btn btn-success">استعادة كلمة المرور</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
