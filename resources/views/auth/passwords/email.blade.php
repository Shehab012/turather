@extends('frontend.layouts.layout')
@section('title')
استعادة كلمة المرور
@endsection
@section('content')
<div class="page-content">
    <div class="top-page">
        <h1>استعادة كلمة المرور</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{'/'}}">الرئيسية</a>
            </li>
            <li class="active">استعادة كلمة المرور</li>
        </ol>
    </div>
    <div class="about">
        <div class="container">
            <div class="all">
<!--                <div class="text-center">
                    <h1>استعادة كلمة المرور</h1>

                </div>-->
                <div class="row">

                    <div class="contact-form">

                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        <form  role="form" method="POST" action="{{ url('/password/email') }}">
                            {{ csrf_field() }}
                            <div style="    margin-top: 63px;" class="col-md-12{{ $errors->has('email') ? ' has-error' : '' }}"> 
                                <div class="form-group  has-feedback">
                                    <label class="control-label" for="p1">البريد الألكترونى</label>
                                    <span class="glyphicon glyphicon-envelope form-control-feedback " aria-hidden="true"></span>

                                    <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-6">
                                <button class="btn btn-success">استعادة كلمة المرور</button>
                            </div>


                        </form>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
