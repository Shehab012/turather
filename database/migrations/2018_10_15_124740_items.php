<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Items extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('s_sub_id')->unsigned();            
            $table->string('title_ar');
            $table->string('title_en');
            $table->text('desc_ar')->nullable();
            $table->text('desc_ar_mini')->nullable();
            $table->text('desc_en')->nullable();
            $table->text('desc_en_mini')->nullable();
            $table->string('seo_ar')->nullable();
            $table->string('seo_en')->nullable();
            $table->text('image');
            $table->string('feature')->default('2');
            $table->string('viewers')->nullable();
            $table->string('status');
            $table->string('subcat_slogen_ar');
            $table->string('subcat_slogen_en');
            $table->timestamps();
            
            // $table->foreign('s_sub_id')->references('id')->on('subofsub')->onDelete('cascade');             
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
