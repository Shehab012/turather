<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_title_ar');
            $table->string('product_title_en');
            $table->text('product_small_desc_ar');
            $table->text('product_small_desc_en');
            $table->text('product_desc_ar');
            $table->text('product_desc_en');
            $table->string('product_type');
            $table->integer('product_order');
            $table->text('image');
            $table->decimal('price',7,2);
            $table->string('product_slogan_ar');
            $table->string('product_slogan_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
