<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Subofsub extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subofsub', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('sub_cat_id')->unsigned();
            $table->string('sub_of_sub_name_ar');
            $table->string('sub_of_sub_name_en');
            $table->text('desc_ar')->nullable();
            $table->text('desc_en')->nullable();
            $table->string('seo_ar')->nullable();
            $table->string('seo_en')->nullable();
            $table->string('status')->default('1');
            $table->integer('viewers')->nullable();
            $table->integer('order')->nullable();
            $table->string('s_s_slogan_ar');
            $table->string('s_s_slogan_en');
            $table->timestamps();
            
            $table->foreign('sub_cat_id')->references('id')->on('sub_cat')->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subofsub');
    }
}
