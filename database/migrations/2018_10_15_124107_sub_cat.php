<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubCat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_cat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('main_cat_id')->unsigned();
            $table->string('sub_cat_name_ar');
            $table->string('sub_cat_name_en');
            $table->text('cat_desc_ar')->nullable();
            $table->text('cat_desc_en')->nullable();
            $table->string('seo_ar')->nullable();
            $table->string('seo_en')->nullable();
            $table->string('order');
            $table->string('status');
            $table->string('subcat_slogen_ar');
            $table->string('subcat_slogen_en');
            $table->timestamps();

            $table->foreign('main_cat_id')->references('id')->on('main_cat')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_cat');
    }
}
