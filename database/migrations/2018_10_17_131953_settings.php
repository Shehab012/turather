<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('website_name_ar');
            $table->string('website_name_en');
            $table->text('website_address_ar');
            $table->text('website_address_en');
            $table->text('website_about_ar');
            $table->text('website_about_en');
            $table->string('website_email');
            $table->string('website_phone');
            $table->text('website_logo_ar');
            $table->text('website_logo_en');
            $table->text('website_logo_footer_ar');
            $table->text('website_logo_footer_en');
            $table->text('website_icon');
            $table->text('website_keywor_ar');
            $table->text('website_keywor_en');
            $table->text('website_desc_ar');
            $table->text('website_desc_en');
            $table->string('website_facebook')->nullable();
            $table->string('website_twitter')->nullable();
            $table->string('website_google')->nullable();
            $table->string('website_youtube')->nullable();
            $table->string('website_linked_in')->nullable();
            $table->string('website_instgram')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
