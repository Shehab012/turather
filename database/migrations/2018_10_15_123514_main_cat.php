<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MainCat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_cat', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_name_ar');
            $table->string('cat_name_en');
            $table->text('cat_desc_ar')->nullable();
            $table->text('cat_desc_en')->nullable();
            $table->string('order');
            $table->string('status')->default('1');
            $table->string('seo_ar')->nullable();
            $table->string('seo_en')->nullable();
            $table->string('subcat_slogen_ar');
            $table->string('subcat_slogen_en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_cat');
    }
}
