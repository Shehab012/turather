-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 08, 2018 at 02:22 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `turather_DB`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `title_ar`, `title_en`, `desc_ar`, `desc_en`, `image`, `created_at`, `updated_at`) VALUES
(1, 'fbfgdbfdbdbdfdfs', 'dfbdfsbdffdsb', '<p>dfbdfbdfbdfbfs</p>', '<p>dfbdfbdfbfdbdfb</p>', 'slider3_1541005992.jpg', '2018-10-31 15:13:12', '2018-10-31 15:13:12');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'desactive',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unwatched',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `massege` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unwatched',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_ar` text COLLATE utf8mb4_unicode_ci,
  `desc_ar_mini` text COLLATE utf8mb4_unicode_ci,
  `desc_en` text COLLATE utf8mb4_unicode_ci,
  `desc_en_mini` text COLLATE utf8mb4_unicode_ci,
  `seo_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `viewers` int(11) DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcat_slogen_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcat_slogen_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `sub_cat_id`, `title_ar`, `title_en`, `desc_ar`, `desc_ar_mini`, `desc_en`, `desc_en_mini`, `seo_ar`, `seo_en`, `image`, `feature`, `viewers`, `status`, `subcat_slogen_ar`, `subcat_slogen_en`, `created_at`, `updated_at`) VALUES
(14, 12, 'التراث العالمي', 'world heritage', '<p>تعتبر الآثار إرثاً تاريخيّاً شاهداً على الحضارات القديمة، والطقوس التي كانوا يمارسونها في تلك المعالم، وقد شيّدت هذه المعالم لأسباب مختلفة لكن تنحصر في حاجة تلك الحضارات للاستقرار، وحماية ممتلكاتها من الهجمات المختلفة، وأهمّ ما يميّز هذه الآثار تصميمها المعماريّ الجذّاب الهندسيّ الصنع، ويوجد العديد من سكان العالم يزورون هذه الأماكن بغية رؤية هذا التاريخ الجميل، وتقوم وزارت الآثار في البلدان المختلفة بالمحافظة على هذه المعالم، وترميمها لكي تبقى شامخةً في ربوعها.<br />\r\n&nbsp;</p>', 'تعتبر الآثار إرثاً تاريخيّاً شاهداً على الحضارات القديمة، والطقوس التي كانوا يمارسونها في تلك المعالم', '<p>The monuments are a historical heritage that bears witness to the ancient civilizations and rituals that they practiced in these monuments. These monuments were constructed for various reasons, but are limited to the need for these civilizations to settle and protect their properties from various attacks. The most important characteristic of these monuments is their attractive architectural design. Of the world&#39;s population visit these places in order to see this beautiful history, and the ministries of antiquities in the various countries to preserve these monuments, and restoration to remain high in the region.</p>', 'The monuments are a historical heritage that bears witness to the ancient civilizations and rituals that they p', 'اهميه التراث العالمي ,التراث العالمي', 'The importance of world heritage,world heritage', 'India_Rajasthan_Udaipur_City-Palace_1541672303.jpg', '1', NULL, '1', 'التراث-العالمي', 'world-heritage', '2018-11-08 08:18:23', '2018-11-08 08:18:23'),
(15, 11, 'التراث الاسلامي', 'Islamic Heritage', '<p>التراث الإسلامي، مصطلح شامل يتسع لكل ما له علاقة بالإسلام&nbsp;من نصوصالقران والسنه النبويه&nbsp;&nbsp;واجتهادات العلماء السابقين في فهم هذه النصوص وتطبيقها على الواقع، وقد حصل خلاف حول ما إذا كان هذا التراث دين مقدسا&nbsp;&nbsp;يجب الالتزام به، أو نصوصا واجتهادات مرتبطة بأزمانها وأماكنها الغابرة، تعامل على أنها تاريخ&nbsp;ينقل لنا تجربة بشرية قابلة للنقد والنقض والتعديل والتطوير بما يتناسب مع الزمان والمكان والظروف الخاصة بكل عصر&nbsp;.</p>\r\n\r\n<p>وهو لا يقتصر بالضرورة على الإنتاج المعرفي فيالعلوم الشرعيه&nbsp;&nbsp;وحدها كالتفسيروالحديث والفقه&nbsp;&nbsp;ونحو ذلك، بل يتسع ليشمل كل ما خلَّفه العلماء&nbsp;<a href=\"https://ar.wikipedia.org/wiki/%D8%A7%D9%84%D9%85%D8%B3%D9%84%D9%85%D9%88%D9%86\">ا</a>لمسلمون&nbsp;عبر العصور من مؤلفات في مختلف فروع المعرفه، وبشتى اللغات، وفي كل بقعة من بقاع الأرض بلغتها دعوةُ الإسلام. ويمكن تعريف التراث الإسلامى على أنه كل ما خلفه الأسلاف المسلمون من عقيدة دينية (<a href=\"https://ar.wikipedia.org/wiki/%D8%A7%D9%84%D9%82%D8%B1%D8%A2%D9%86\">ا</a>لقرآن&nbsp;والسنه) وعطاءات حضارية (ماديه&nbsp;ومعنوية).</p>', 'التراث الإسلامي، مصطلح شامل يتسع لكل ما له علاقة بالإسلام من نصوصالقران والسنه النبويه  واجتهادات العلماء الساب', '<p>The Islamic heritage, a comprehensive term for all that has to do with Islam from Nusalqaran and Sunnah and the jurisprudence of the former scholars to understand these texts and apply them to reality, and there was disagreement over whether this heritage sacred religion must be adhered to, or texts and jurisprudence linked to the times and places of the past, As a history that conveys to us a human experience that is capable of criticism, criticism, modification and development in accordance with the time, place and circumstances of each era.</p>\r\n\r\n<p>It is not limited to the production of knowledge in Shari&#39;a sciences alone, such as interpretation, modernity, jurisprudence, and so on. It extends to include all the works of Muslim scholars throughout the ages, from books in different branches of knowledge, in various languages and in every part of the world. Islamic heritage can be defined as all that is left behind by Muslim ancestors from a religious creed (Quran and Sunnah) and cultural (material and moral) sacrifices.<br />\r\n&nbsp;</p>', 'The Islamic heritage, a comprehensive term for all that has to do with Islam from Nusalqaran and Sunnah and the', 'التراث الاسلامي', 'Islamic Heritage', 'download_1541673009.jpg', '1', 2, '1', 'التراث-الاسلامي', 'islamic-heritage', '2018-11-08 08:30:09', '2018-11-08 08:30:24'),
(16, 9, 'اللباس', 'Dress Up', '<p>الزى الوطني (يعرف أيضا بالملابس الوطنية أو الزي التقليدي أو الزى الشعبي أو الملابس التقليدية) هو زي يعبر عن هوية منطقة جغرافية معينة أو فترة زمنية ما. كما يمكن أن يكون تعبيرا عن مكانة اجتماعية أو وضع اجتماعي أو ديني. عادة ما يتكون الزي من طقمين: طقم للمناسبات اليومية، و أخر يستخدم للاحتفالات والرسميات.</p>\r\n\r\n<p>في المناطق التي تستخدم الملابس الغربية للاستخدام العام، يتم استخدام الأزياء الشعبية في المناسبات الخاصة و الاحتفالات، خاصة تلك المرتبطة بالعادات والتقاليد أو الزفاف.</p>', 'الزى الوطني (يعرف أيضا بالملابس الوطنية أو الزي التقليدي أو الزى الشعبي أو الملابس التقليدية) هو زي يعبر عن هو', '<p>National costume (also known as national clothing, traditional dress, folk costume or traditional clothing) is a costume that reflects the identity of a particular geographical area or period of time. It can also be an expression of social status or social or religious status. The costume usually consists of two sets: a set for everyday occasions, and another for ceremonies and formalities.</p>\r\n\r\n<p>In areas where Western clothing is used for public use, popular costumes are used for special events and celebrations, especially those associated with customs, traditions or weddings.</p>', 'National costume (also known as national clothing, traditional dress, folk costume or traditional clothing) is', 'التراث الوطني ,الزي التقليدي ,اللباس الوطني', 'National Dress,Traditional Dress,National Heritage', '-اللباس-التقليدي-الجزائري-و-الممارسات-الشعبية-...أداة-ثقافية-للحفاظ-و-تثمين-التراث-الوطني_1541673468.jpg', '1', 5, '1', 'اللباس', 'dress-up', '2018-11-08 08:37:48', '2018-11-08 10:07:10'),
(17, 9, 'الطعام', 'the food', '<p>هناك ضرورة لإدخال قصة الإنسان مع الغذاء فى مناهجنا الدراسية، فهناك أربعة مستجدات ابتكرها الإنسان، جعلت تفاعله مع الحياة أكثر تداخلا، وهي: أدوات الطرق والقص، و استعمال النار فى الطبخ، و أدوات الصيد، وأخيرا تحول الافراد إلى مجتمعات بشرية لديها ثقافة تبادل الخبرات وتنميتها.<br />\r\nوهذه الاخيرة قائمة على معادلة إنسانية هي: ملل الإنسان من تكرار نفس الأشياء، وهى معادلة تفرق بين الإنسان والحيوان، وهو ما يدفعه إلى الابتكار والتنويع فى حياته.<br />\r\nفهناك دلائل أثرية تشير إلى أنه خلال الفترة المتأخرة من العصر الحجرى القديم قام الناس بطهى الطعام، وهو مايمكن أن يعد بسهولة أول عملية كيميائية</p>', 'هناك ضرورة لإدخال قصة الإنسان مع الغذاء فى مناهجنا الدراسية، فهناك أربعة مستجدات ابتكرها الإنسان، جعلت تفاعله م', '<p>There is a need to introduce the story of man with food in our curriculum, there are four innovations created by man, made his interaction with life more interrelated: the tools of roads and cutting, and the use of fire in cooking, and fishing tools, and finally turning people into human communities have a culture of sharing experiences And development.<br />\r\nThe latter is based on a human equation: human boredom of repetition of the same things, an equation that distinguishes between man and animal, which drives him to innovate and diversify his life.</p>', 'There is a need to introduce the story of man with food in our curriculum, there are four innovations created b', 'الطعام ,التراث الوطني', 'National Heritage,the food', 'download_1541674318.jpg', '1', 5, '1', 'الطعام', 'the-food', '2018-11-08 08:51:58', '2018-11-08 09:27:36'),
(18, 9, 'المباني', 'Buildings', '<p>&nbsp;يعد التراث العمراني أحد الرموز الأساسية لتطور الإنسان عبر التاريخ، ويعبر عن القدرات التي وصل إليها الإنسان في التغلب على بيئته المحيطة، والتراث يعني توريث حضارات السلف للخلف ولا يقتصر ذلك على اللغة أو الأدب والفكر فقط، بل يعم جميع العناصر المادية والوجدانية للمجتمع من فكر وفلسفة ودين وعلم وفن وعمران.<br />\r\n&nbsp;<br />\r\nويعتبر العمران أحد أهم العناصر الأساسية للتراث ويتميز عن غيرة من العناصر التراث بوجوده المادي مجددا بذلك وجود حضارات الأجيال السابقة بصورة مباشرة لا تقبل الشك أو الجدل. كما يبرز تتابع لتجارب وقيم حضارية واجتماعية ودينية بين الأجيال.</p>', ' يعد التراث العمراني أحد الرموز الأساسية لتطور الإنسان عبر التاريخ، ويعبر عن القدرات التي وصل إليها الإنسان في', '<p>Urban heritage is one of the basic symbols of man&#39;s development throughout history. It reflects the abilities that humans have achieved in overcoming their surrounding environment. Heritage means inheriting the civilizations of the forefathers, not only language, literature and thought, but all the material and emotional elements of the society from thought and philosophy. Religion, science, art and architecture.<br />\r\n&nbsp;<br />\r\nUrbanism is one of the most important elements of heritage and is characterized by a variety of heritage elements in its material existence, thus renewing the existence of civilizations of previous generations directly and without doubt or controversy. It also highlights a sequence of intergenerational, social and religious experiences and values.</p>', 'Urban heritage is one of the basic symbols of man\'s development throughout history. It reflects the abilities t', 'المباني', 'Buildings', '1449394349_6bb4bb2a-8820-4e01-944d-39d634a90519_cx0_cy9_cw0_mw1024_s_n_r1_1541674567.jpg', '1', 3, '1', 'المباني', 'buildings', '2018-11-08 08:56:07', '2018-11-08 09:03:15'),
(19, 9, 'ادوات حرفيه', 'Craft Tools', '<p>تحتل الحرف اليدوية والصناعات التقليدية مساحة واسعة من التراث المصرى يعتمد فيها &nbsp;الصانع على مهاراته الفردية الذهنية واليدوية ، باستخدام الخامات الأولية المتوفرة في البيئة الطبيعية المحلية أو الخامات الأولية المستوردة .</p>', 'تحتل الحرف اليدوية والصناعات التقليدية مساحة واسعة من التراث المصرى', '<p>Handicrafts and traditional industries occupy a large area of Egyptian heritage where the artist relies on his individual mental and manual skills, using raw materials available in the local natural environment or raw raw materials imported.</p>', 'Handicrafts and traditional industries occupy a large area of Egyptian heritage where the artist relies on his', 'الادوات الحرفيه ,التراث الوطني', 'الادوات الحرفيه , التراث الوطني', 'download_1541675791.jpg', '1', NULL, '1', 'ادوات-حرفيه', 'craft-tools', '2018-11-08 09:16:31', '2018-11-08 09:16:31'),
(20, 9, 'وسائل المواصلات', 'transportation', '<p>يعد النقل البري من وسائل النقل البدائيّة، حيث أنشأ البشر العديد من الطرق لنقل بضائعهم، وللسفر من مكان إلى آخر، ثمّ دجنوا العديد من الحيوانات، مثل الحمار، والحصان، والثور، والتي شكلت عنصراً مهمّاً في عملية النقل، بالإضافة إلى أنهم اتجهوا إلى توسعة الطرق بعد النموّ التجاري، بهدف جعلها ملائمة لحركة الحيوانات، ثم استخدموا العربات البدائية، والتي كانت عبارة عن عصيتين مربوطتين في ظهر الدابة</p>', 'يعد النقل البري من وسائل النقل البدائيّة، حيث أنشأ البشر العديد من الطرق لنقل بضائعهم، وللسفر من مكان إلى آخر،', '<p>Land transport is a primitive mode of transport. Humans have created many ways to transport their goods, to travel from one place to another, and then drowned many animals, such as donkey, horse and bull, which were an important element in the transport process. Roads after commercial growth, with the aim of making them suitable for the movement of animals, and then used the primitive vehicles, which were two rods tied to the back of the animal</p>', 'Land transport is a primitive mode of transport. Humans have created many ways to transport their goods, to tra', 'وسائل المواصلات ,التراث الوطني', 'transportation,التراث الوطني', '_هي_وسائل_النقل_القديمة_1541676449.jpg', '1', 1, '1', 'وسائل-المواصلات', 'transportation', '2018-11-08 09:27:29', '2018-11-08 09:27:39'),
(21, 9, 'عادات وتقاليد', 'Customs and traditions', '<p>في حياتنا الكثير من الأمور التي اعتدنا على وجودها بشكلٍ أو بآخر، رغم أنّ هذا الوجود كان غير مبرر في كثير من الأحوال، ولو نظرنا حولنا جيداً، لنظرنا إلى بعض الأمور التي لا يمكن أن نتخلى عنها، على الرغم من أنها أصبحت من القدم، وما عاد لها أي جذر في حياتنا اليومية على وجه الخصوص، وهذه الأمور على الرغم من قدمها، وغرابتها في أكثر الأحيان إلّا أنّنا اعتدناها، حتى أصبحت جزءً لا يتجزأ من حياتنا على الإطلاق</p>', 'في حياتنا الكثير من الأمور التي اعتدنا على وجودها بشكلٍ أو بآخر، رغم أنّ هذا الوجود كان غير مبرر في كثير من الأ', '<p>There are many things in our life that we are used to in one way or another, although this presence was not justified in many cases, if we looked around well, to look at some things that can not be abandoned, although it has become a foot, and She returned to her any root in our daily lives in particular, and these things though her feet, and their strangeness more often than we have become accustomed to, have become an integral part of our lives at all</p>', 'There are many things in our life that we are used to in one way or another, although this presence was not jus', 'عادات وتقاليد', 'Customs and traditions', 'images_1541677815.jpg', '1', NULL, '1', 'عادات-وتقاليد', 'customs-and-traditions', '2018-11-08 09:50:15', '2018-11-08 09:50:15'),
(22, 9, 'الزراعه', 'Agriculture', '<p>تعد الزراعة من المهن القديمة التي امتهنها أجدادنا، واعتمدوا عليها في تأمين ما يحتاجونه من غذاء لهم ولعائلاتهم، ثم أخذوا يقايضون جيرانهم بما لديهم من محاصيل مقابل ما لديهم، وبذلك ظهر مفهوم تجارة المنتجات الزراعية.</p>', 'تعد الزراعة من المهن القديمة التي امتهنها أجدادنا، واعتمدوا عليها في تأمين ما يحتاجونه من غذاء لهم ولعائلاتهم', '<p>Agriculture is one of the old professions that our forefathers have depended on, and they have relied on them to provide food for themselves and their families, and then they have traded their neighbors with their crops for what they have.<br />\r\n&nbsp;</p>', 'Agriculture is one of the old professions that our forefathers have depended on, and they have relied on them t', 'الزراعه', 'Agriculture', 'images_1541678455.jpg', '1', NULL, '1', 'الزراعه', 'agriculture', '2018-11-08 10:00:55', '2018-11-08 10:00:55');

-- --------------------------------------------------------

--
-- Table structure for table `main_cat`
--

CREATE TABLE `main_cat` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_desc_ar` text COLLATE utf8mb4_unicode_ci,
  `cat_desc_en` text COLLATE utf8mb4_unicode_ci,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `seo_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subcat_slogen_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcat_slogen_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_cat`
--

INSERT INTO `main_cat` (`id`, `cat_name_ar`, `cat_name_en`, `cat_desc_ar`, `cat_desc_en`, `order`, `status`, `seo_ar`, `seo_en`, `subcat_slogen_ar`, `subcat_slogen_en`, `created_at`, `updated_at`) VALUES
(2, 'التراث', 'Heritage', NULL, NULL, '3', '1', NULL, NULL, 'السيارات', 'cars', '2018-10-31 06:38:35', '2018-11-08 06:47:25'),
(3, 'الآثار', 'Archaeology', NULL, NULL, '2', '1', NULL, NULL, 'الدرجات-الناريه', 'motorcycle', '2018-10-31 06:40:06', '2018-11-08 06:48:31');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(96, '2014_10_12_000000_create_users_table', 1),
(97, '2014_10_12_100000_create_password_resets_table', 1),
(98, '2018_10_15_123514_main_cat', 1),
(99, '2018_10_15_124107_sub_cat', 1),
(100, '2018_10_15_124740_items', 1),
(101, '2018_10_15_125644_comments', 1),
(102, '2018_10_17_131953_settings', 1),
(103, '2018_10_23_112418_about_table', 1),
(104, '2018_10_23_112655_contact_us_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_small_desc_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_small_desc_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_desc_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_desc_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_order` int(11) DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(7,2) NOT NULL,
  `viewers` int(11) NOT NULL DEFAULT '0',
  `product_slogan_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_slogan_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_title_ar`, `product_title_en`, `product_small_desc_ar`, `product_small_desc_en`, `product_desc_ar`, `product_desc_en`, `product_type`, `product_order`, `image`, `price`, `viewers`, `product_slogan_ar`, `product_slogan_en`, `created_at`, `updated_at`) VALUES
(1, 'تمثال الحريه', 'horas', 'How such documents are actually stored depends on the file format. For example', 'How such documents are actually stored depends on the file format. For example', '<p>In&nbsp;<a href=\"https://en.wikipedia.org/wiki/Word_processing\">word processing</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Desktop_publishing\">desktop publishing</a>, a&nbsp;<strong>hard return</strong>&nbsp;or&nbsp;<strong>paragraph break</strong>&nbsp;indicates a new paragraph, to be distinguished from the&nbsp;<strong>soft return</strong>&nbsp;at the end of a line internal to a paragraph. This distinction allows&nbsp;<a href=\"https://en.wikipedia.org/wiki/Word_wrap\">word wrap</a>&nbsp;to automatically re-flow text as it is edited, without losing paragraph breaks. The software may apply vertical whitespace or indenting at paragraph breaks, depending on the selected style.</p>\r\n\r\n<p>How such documents are actually stored depends on the&nbsp;<a href=\"https://en.wikipedia.org/wiki/File_format\">file format</a>. For example,&nbsp;<a href=\"https://en.wikipedia.org/wiki/HTML\">HTML</a>&nbsp;uses the</p>\r\n\r\n<p>tag as a paragraph container. In&nbsp;<a href=\"https://en.wikipedia.org/wiki/Plaintext\">plaintext</a>&nbsp;files, there are two common formats. Pre-formatted text will have a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Newline\">newline</a>&nbsp;at the end of every physical line, and two newlines at the end of a paragraph, creating a blank line. An alternative is to only put newlines at the end of each paragraph, and leave word wrapping up to the application that displays or processes the text.</p>', '<p>In&nbsp;<a href=\"https://en.wikipedia.org/wiki/Word_processing\">word processing</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Desktop_publishing\">desktop publishing</a>, a&nbsp;<strong>hard return</strong>&nbsp;or&nbsp;<strong>paragraph break</strong>&nbsp;indicates a new paragraph, to be distinguished from the&nbsp;<strong>soft return</strong>&nbsp;at the end of a line internal to a paragraph. This distinction allows&nbsp;<a href=\"https://en.wikipedia.org/wiki/Word_wrap\">word wrap</a>&nbsp;to automatically re-flow text as it is edited, without losing paragraph breaks. The software may apply vertical whitespace or indenting at paragraph breaks, depending on the selected style.</p>\r\n\r\n<p>How such documents are actually stored depends on the&nbsp;<a href=\"https://en.wikipedia.org/wiki/File_format\">file format</a>. For example,&nbsp;<a href=\"https://en.wikipedia.org/wiki/HTML\">HTML</a>&nbsp;uses the</p>\r\n\r\n<p>tag as a paragraph container. In&nbsp;<a href=\"https://en.wikipedia.org/wiki/Plaintext\">plaintext</a>&nbsp;files, there are two common formats. Pre-formatted text will have a&nbsp;<a href=\"https://en.wikipedia.org/wiki/Newline\">newline</a>&nbsp;at the end of every physical line, and two newlines at the end of a paragraph, creating a blank line. An alternative is to only put newlines at the end of each paragraph, and leave word wrapping up to the application that displays or processes the text.</p>', '1', 1, 'ea9ce5a900ae9a639967ebfbf21ad78e741d76b2-021116120523_1541496419.jpg', '200.00', 6, 'تمثال-الحريه', 'horas', '2018-11-05 14:21:11', '2018-11-07 10:19:17'),
(2, 'ايزيس', 'Ezice', 'How such documents are actually stored depends on the file format. For example', 'How such documents are actually stored depends on the file format. For example', '<p>Paragraphs are commonly numbered using the decimal system, where (in books) the integral part of the decimal represents the number of the chapter and the fractional parts are arranged in each chapter in order of magnitude. Thus in Whittaker and Watson&#39;s 1921&nbsp;<em><a href=\"https://en.wikipedia.org/wiki/A_Course_of_Modern_Analysis\">A Course of Modern Analysis</a></em>, chapter 9 is devoted to Fourier Series; within that chapter &sect;9.6 introduces Riemann&#39;s theory, the following section &sect;9.61 treats an associated function, following &sect;9.62 some properties of that function, following &sect;9.621 a related lemma, while &sect;9.63 introduces Riemann&#39;s main theorem, and so on. Whittaker and Watson attribute this system of numbering to&nbsp;<a href=\"https://en.wikipedia.org/wiki/Giuseppe_Peano\">Giuseppe Peano</a>&nbsp;on their &quot;Contents&quot; page, although this attribution does not seem to be widely credited elsewhere.<a href=\"https://en.wikipedia.org/wiki/Paragraph#cite_note-7\">[7]</a></p>', '<p>Paragraphs are commonly numbered using the decimal system, where (in books) the integral part of the decimal represents the number of the chapter and the fractional parts are arranged in each chapter in order of magnitude. Thus in Whittaker and Watson&#39;s 1921&nbsp;<em><a href=\"https://en.wikipedia.org/wiki/A_Course_of_Modern_Analysis\">A Course of Modern Analysis</a></em>, chapter 9 is devoted to Fourier Series; within that chapter &sect;9.6 introduces Riemann&#39;s theory, the following section &sect;9.61 treats an associated function, following &sect;9.62 some properties of that function, following &sect;9.621 a related lemma, while &sect;9.63 introduces Riemann&#39;s main theorem, and so on. Whittaker and Watson attribute this system of numbering to&nbsp;<a href=\"https://en.wikipedia.org/wiki/Giuseppe_Peano\">Giuseppe Peano</a>&nbsp;on their &quot;Contents&quot; page, although this attribution does not seem to be widely credited elsewhere.<a href=\"https://en.wikipedia.org/wiki/Paragraph#cite_note-7\">[7]</a></p>', '1', 2, 'item_XL_22635816_31288642_1541511290.jpg', '49.96', 3, 'ايزيس', 'ezice', '2018-11-06 11:34:50', '2018-11-07 10:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `website_name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_address_ar` text COLLATE utf8mb4_unicode_ci,
  `website_address_en` text COLLATE utf8mb4_unicode_ci,
  `website_about_ar` text COLLATE utf8mb4_unicode_ci,
  `website_about_en` text COLLATE utf8mb4_unicode_ci,
  `website_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_logo_ar` text COLLATE utf8mb4_unicode_ci,
  `website_logo_en` text COLLATE utf8mb4_unicode_ci,
  `website_logo_footer_ar` text COLLATE utf8mb4_unicode_ci,
  `website_logo_footer_en` text COLLATE utf8mb4_unicode_ci,
  `website_icon` text COLLATE utf8mb4_unicode_ci,
  `website_keywor_ar` text COLLATE utf8mb4_unicode_ci,
  `website_keywor_en` text COLLATE utf8mb4_unicode_ci,
  `website_desc_ar` text COLLATE utf8mb4_unicode_ci,
  `website_desc_en` text COLLATE utf8mb4_unicode_ci,
  `website_facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_linked_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_instgram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `website_name_ar`, `website_name_en`, `website_address_ar`, `website_address_en`, `website_about_ar`, `website_about_en`, `website_email`, `website_phone`, `website_logo_ar`, `website_logo_en`, `website_logo_footer_ar`, `website_logo_footer_en`, `website_icon`, `website_keywor_ar`, `website_keywor_en`, `website_desc_ar`, `website_desc_en`, `website_facebook`, `website_twitter`, `website_google`, `website_youtube`, `website_linked_in`, `website_instgram`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'linkinpark_1541409280.png', 'linkinpark_1541322674_1541516515.png', 'webicon _1541066245_1541516515.png', 'الكلمات المفتاحية  عربى', 'الكلمات المفتاحية انجليزى', 'وصف الموقع عربى \r\n وصف الموقع عربى', 'وصف الموقع انجليزى \r\nوصف الموقع انجليزى', NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-05 07:07:27', '2018-11-06 13:01:55');

-- --------------------------------------------------------

--
-- Table structure for table `sub_cat`
--

CREATE TABLE `sub_cat` (
  `id` int(10) UNSIGNED NOT NULL,
  `main_cat_id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_cat_name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_desc_ar` text COLLATE utf8mb4_unicode_ci,
  `cat_desc_en` text COLLATE utf8mb4_unicode_ci,
  `seo_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcat_slogen_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcat_slogen_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_cat`
--

INSERT INTO `sub_cat` (`id`, `main_cat_id`, `sub_cat_name_ar`, `sub_cat_name_en`, `cat_desc_ar`, `cat_desc_en`, `seo_ar`, `seo_en`, `order`, `status`, `subcat_slogen_ar`, `subcat_slogen_en`, `created_at`, `updated_at`) VALUES
(9, 2, 'التراث الوطني', 'National Heritage', NULL, NULL, NULL, NULL, '1', '1', 'التراث-الوطني', 'national-heritage', '2018-11-08 07:03:41', '2018-11-08 07:03:41'),
(10, 2, 'التراث العربي', 'Arab Heritage', NULL, NULL, NULL, NULL, '2', '1', 'التراث-العربي', 'arab-heritage', '2018-11-08 07:04:35', '2018-11-08 07:04:35'),
(11, 2, 'التراث الإسلامي', 'Islamic Heritage', NULL, NULL, NULL, NULL, '3', '1', 'التراث-الإسلامي', 'islamic-heritage', '2018-11-08 07:05:22', '2018-11-08 07:05:22'),
(12, 2, 'التراث العالمي', 'The global heritage', NULL, NULL, NULL, NULL, '4', '1', 'التراث-العالمي', 'the-global-heritage', '2018-11-08 07:06:18', '2018-11-08 07:06:42'),
(13, 3, 'آثار وطنيه', 'National antiquities', NULL, NULL, NULL, NULL, '5', '1', 'آثار-وطنيه', 'national-antiquities', '2018-11-08 07:21:53', '2018-11-08 07:21:53'),
(14, 3, 'اثار عربيه', 'Arab Antiquities', NULL, NULL, NULL, NULL, '6', '1', 'اثار-عربيه', 'arab-antiquities', '2018-11-08 07:22:46', '2018-11-08 07:22:46'),
(15, 3, 'آثار اسلاميه', 'Islamic Antiquities', NULL, NULL, NULL, NULL, '7', '1', 'آثار-اسلاميه', 'islamic-antiquities', '2018-11-08 07:23:52', '2018-11-08 07:23:52'),
(16, 3, 'آثار عالميه', 'World heritage', NULL, NULL, NULL, NULL, '8', '1', 'آثار-عالميه', 'world-heritage', '2018-11-08 07:26:04', '2018-11-08 07:26:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$ntQzhlaFIYXxIc1Cb59eIOTSo6bj3B4pigIz.LWW.qT8IjveZYjQO', '1Ti4JZap23DZdvzufE0erAbo92UKqavhG8RL5Jk7XEaqBEavkhs7J4IYfSLs', '2018-10-28 13:07:58', '2018-10-28 13:07:58'),
(3, 'shehab hosny hassan', 'shehab_xc@yahoo.com', '$2y$10$UxHUOp7SZmCA9U2E3Ydege54vApbfmlQfH6LxuP9veeFOd3NN6LyG', NULL, '2018-11-06 14:08:39', '2018-11-06 14:08:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_item_id_foreign` (`item_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_sub_cat_id_foreign` (`sub_cat_id`);

--
-- Indexes for table `main_cat`
--
ALTER TABLE `main_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_cat`
--
ALTER TABLE `sub_cat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_cat_main_cat_id_foreign` (`main_cat_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `main_cat`
--
ALTER TABLE `main_cat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sub_cat`
--
ALTER TABLE `sub_cat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_sub_cat_id_foreign` FOREIGN KEY (`sub_cat_id`) REFERENCES `sub_cat` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_cat`
--
ALTER TABLE `sub_cat`
  ADD CONSTRAINT `sub_cat_main_cat_id_foreign` FOREIGN KEY (`main_cat_id`) REFERENCES `main_cat` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
